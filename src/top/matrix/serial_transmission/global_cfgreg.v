`include "pixel_matrix_parameters"
`include "column_ctl_params"
`timescale `TIMESCALE

module global_cfgreg #(
		parameter CFGREG_REG_NUMBER	= `GLOBAL_CFGREG_REG_NUMBER,
		parameter CFGREG_EN_WIDTH 	= $clog2(CFGREG_REG_NUMBER),
		parameter CFGREG_WIDTH 		= `GLOBAL_CFGREG_WIDTH
	)
	(
		input wire 							clk,
		input wire [CFGREG_EN_WIDTH-1:0]				en,		//clock enable
		input wire [CFGREG_WIDTH-1:0]					din,
		input wire 							rst,
		input wire 							write,
		output reg [(CFGREG_REG_NUMBER*CFGREG_WIDTH)-1:0] 		cfg
		);
	
genvar i;
generate
for(i=0;i<CFGREG_REG_NUMBER;i=i+1)begin
	//--Write to CfgReg--//
	always @(posedge clk) begin
		if(rst) begin
				cfg[((i+1)*CFGREG_WIDTH)-1:(i*CFGREG_WIDTH)] <= {CFGREG_WIDTH{1'b0}};
		end else begin
				cfg[((i+1)*CFGREG_WIDTH)-1:(i*CFGREG_WIDTH)] <= ((en == i) & write) ? din : cfg[((i+1)*CFGREG_WIDTH)-1:(i*CFGREG_WIDTH)];
		end
	end
end
endgenerate

endmodule
