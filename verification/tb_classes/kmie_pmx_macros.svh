
// number of bits in the command; cmd = header + cmd + payload
`define KMIE_PMX_COMMAND_LENGTH 16
`define KMIE_PMX_HEADER_LENGTH 2 
`define KMIE_PMX_PAYLOAD_LENGTH 12
`define KMIE_PMX_REG_SIZE 8
`define KMIE_PMX_REG_COUNT 16
