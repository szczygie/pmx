/******************************************************************************
 * DVT CODE TEMPLATE: sequence library
 * Created by szczygie on Oct 7, 2019
 * uvc_company = kmie, uvc_name = pmx
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_seq_lib
`define IFNDEF_GUARD_kmie_pmx_seq_lib

//------------------------------------------------------------------------------
//
// CLASS: kmie_pmx_base_sequence
//
//------------------------------------------------------------------------------

virtual class kmie_pmx_base_sequence extends uvm_sequence#(kmie_pmx_item_in);

    `uvm_declare_p_sequencer(kmie_pmx_sequencer)

    function new(string name="kmie_pmx_base_sequence");
        super.new(name);
    endfunction : new

    virtual task pre_body();
        uvm_phase starting_phase = get_starting_phase();
        if (starting_phase!=null) begin
            `uvm_info(get_type_name(),
                $sformatf("%s pre_body() raising %s objection",
                    get_sequence_path(),
                    starting_phase.get_name()), UVM_MEDIUM)
            starting_phase.raise_objection(this);
        end
    endtask : pre_body

    virtual task post_body();
        uvm_phase starting_phase = get_starting_phase();
        if (starting_phase!=null) begin
            `uvm_info(get_type_name(),
                $sformatf("%s post_body() dropping %s objection",
                    get_sequence_path(),
                    starting_phase.get_name()), UVM_MEDIUM)
            starting_phase.drop_objection(this);
        end
    endtask : post_body

endclass : kmie_pmx_base_sequence

//------------------------------------------------------------------------------
//
// CLASS: kmie_pmx_sequence_init
//
//------------------------------------------------------------------------------

class kmie_pmx_sequence_init extends kmie_pmx_base_sequence;

    // Add local random fields and constraints here

    `uvm_object_utils(kmie_pmx_sequence_init)

    function new(string name="kmie_pmx_sequence_init");
        super.new(name);
    endfunction : new

    virtual task body();
        `uvm_info(get_type_name(), "Executing sequence", UVM_MEDIUM)
        
        // write all the registers with 0
        for(int i = 0; i < `KMIE_PMX_REG_COUNT; i++) begin
            `uvm_do_with(req,
                {
                    req.pmx_cmd == PMX_CMD_WR_REG;
                    req.reg_val == '0;
                    req.reg_addr == i;
                }
            )
        end
        
    endtask : body

endclass : kmie_pmx_sequence_init

//------------------------------------------------------------------------------
//
// CLASS: kmie_pmx_sequence_regs
//
//------------------------------------------------------------------------------

class kmie_pmx_sequence_regs extends kmie_pmx_base_sequence;

    // Add local random fields and constraints here

    `uvm_object_utils(kmie_pmx_sequence_regs)

    function new(string name="kmie_pmx_sequence_regs");
        super.new(name);
    endfunction : new

    virtual task body();
        `uvm_info(get_type_name(), "Executing sequence", UVM_MEDIUM)
        
        // write all the registers with 0
        for(int i = 0; i < `KMIE_PMX_REG_COUNT; i++) begin
            `uvm_do_with(req,
                {
                    req.pmx_cmd == PMX_CMD_WR_REG;
                    req.reg_val == '0;
                    req.reg_addr == i;
                }
            )
        end
        
        // write all the registers with FF
        for(int i = 0; i < `KMIE_PMX_REG_COUNT; i++) begin
            `uvm_do_with(req,
                {
                    req.pmx_cmd == PMX_CMD_WR_REG;
                    req.reg_val == '1;
                    req.reg_addr == i;
                }
            )
        end
        
        // read all the registers 
        for(int i = 0; i < `KMIE_PMX_REG_COUNT; i++) begin
            `uvm_do_with(req, { req.pmx_cmd == PMX_CMD_RD_REG; } )
        end
        
        
        // random read/write
        repeat(100) begin
            `uvm_do_with(req, { req.pmx_cmd == PMX_CMD_WR_REG || req.pmx_cmd == PMX_CMD_RD_REG; } )
            end
        
    endtask : body

endclass : kmie_pmx_sequence_regs




`endif // IFNDEF_GUARD_kmie_pmx_seq_lib
