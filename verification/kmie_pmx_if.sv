/******************************************************************************
 * DVT CODE TEMPLATE: interface
 * Created by szczygie on Oct 7, 2019
 * uvc_company = kmie, uvc_name = pmx
 *******************************************************************************/

//------------------------------------------------------------------------------
//
// INTERFACE: kmie_pmx_if
//
//------------------------------------------------------------------------------
`timescale 1ns/1ps

// Just in case you need them
`include "uvm_macros.svh"

interface kmie_pmx_if(
    clock, // DUT is active posedge
    reset  // reset is active LOW
);

// Just in case you need it
import uvm_pkg::*;

`include "kmie_pmx_macros.svh"
import kmie_pmx_pkg::*;

// Clock and reset signals
input clock;
input reset;

// Flags to enable/disable assertions and coverage
bit checks_enable   =1;
bit coverage_enable =1;

// Declare interface signals here
bit sin             = PMX_CMD_IDLE_STATE;
logic sout;

//------------------------------------------------------------------------------
// Set data sampling delay #1step before the posedge clock and
// setting delay for #1step after posedge clock
//    https://www.verificationguide.com/p/systemverilog-clocking-block.html

clocking cb_drv @(posedge clock); // for driving the DUT
    default input #1ns output #2ns;
    output sin;
endclocking

clocking cb_mon @(posedge clock); // for monitoring
    default input #1ns output #2ns;
    input sin;
    input sout;
endclocking

//------------------------------------------------------------------------------
task send_data( input kmie_pmx_item_in item );
    bit[`KMIE_PMX_COMMAND_LENGTH:0] data;
    begin
        data = {PMX_CMD_HEADER, item.pmx_cmd, item.m_data};
        for(int i = `KMIE_PMX_COMMAND_LENGTH - 1; i>= 0; i--) begin
            @( cb_drv ) sin <= data[i]; // non-blocking to make the correct use of the clocking block
        end
    end
endtask


//------------------------------------------------------------------------------
task get_dut_input_data( output kmie_pmx_item_in collected_item );

    localparam byte CMD_MSB          = `KMIE_PMX_COMMAND_LENGTH - `KMIE_PMX_HEADER_LENGTH - 1;
    localparam byte CMD_LSB          = `KMIE_PMX_COMMAND_LENGTH - `KMIE_PMX_HEADER_LENGTH - 2;

    bit [`KMIE_PMX_COMMAND_LENGTH-1:0] data;

    collected_item                   = kmie_pmx_item_in::type_id::create("collected_item");

    // register data --------------------

    while(cb_mon.sin == PMX_CMD_IDLE_STATE) @( cb_mon ); // wait for header

    data[`KMIE_PMX_COMMAND_LENGTH-1] = cb_mon.sin;

    for(byte i = `KMIE_PMX_COMMAND_LENGTH-2; i >=0; i--)begin
        @( cb_mon ) data[i] = cb_mon.sin;
    end

    @( cb_mon );

    // map bits into the collected_item fields --------------------

    collected_item.m_data            = data[`KMIE_PMX_PAYLOAD_LENGTH-1:0];

    if(! $cast( collected_item.pmx_cmd, data[CMD_MSB:CMD_LSB] ) ) begin
        string message;
        message = $sformatf("Failed to cast command data (%b) into pmx_cmd.", data[CMD_MSB:CMD_LSB]);
        `uvm_error("VIF", { "FAILED: ", message } )
    end

    case(collected_item.pmx_cmd)
        PMX_CMD_WR_REG,
        PMX_CMD_RD_REG: begin
            {collected_item.reg_addr, collected_item.reg_val} = collected_item.m_data;
        end
        default:
            {collected_item.reg_addr, collected_item.reg_val} = '0;
    endcase


endtask
//------------------------------------------------------------------------------
task get_dut_output_data( output kmie_pmx_item_out collected_item );

    localparam byte CMD_MSB          = `KMIE_PMX_COMMAND_LENGTH - `KMIE_PMX_HEADER_LENGTH - 1;
    localparam byte CMD_LSB          = `KMIE_PMX_COMMAND_LENGTH - `KMIE_PMX_HEADER_LENGTH - 2;

    bit [`KMIE_PMX_COMMAND_LENGTH-1:0] data;

    collected_item                   = kmie_pmx_item_out::type_id::create("collected_item");

    // register data --------------------

    while(cb_mon.sout == PMX_CMD_IDLE_STATE) @( cb_mon ); // wait for header

    data[`KMIE_PMX_COMMAND_LENGTH-1] = cb_mon.sout;

    for(byte i = `KMIE_PMX_COMMAND_LENGTH-2; i >=0; i--)begin
        @( cb_mon ) data[i] = cb_mon.sout;
    end

    @( cb_mon );

    // map bits into the collected_item fields --------------------

    collected_item.m_data            = data[`KMIE_PMX_PAYLOAD_LENGTH-1:0];

    if(! $cast( collected_item.pmx_cmd, data[CMD_MSB:CMD_LSB] ) ) begin
        string message;
        message = $sformatf("Failed to cast command data (%b) into pmx_cmd.", data[CMD_MSB:CMD_LSB]);
        `uvm_error("VIF", { "FAILED: ", message } )
    end

    case(collected_item.pmx_cmd)
        PMX_CMD_WR_REG,
        PMX_CMD_RD_REG: begin
            {collected_item.reg_addr, collected_item.reg_val} = collected_item.m_data;
        end
        default:
            {collected_item.reg_addr, collected_item.reg_val} = '0;
    endcase

endtask

endinterface : kmie_pmx_if
