/******************************************************************************
 * DVT CODE TEMPLATE: monitor
 * Created by szczygie on Oct 7, 2019
 * uvc_company = kmie, uvc_name = pmx
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_monitor_base
`define IFNDEF_GUARD_kmie_pmx_monitor_base

//------------------------------------------------------------------------------
//
// CLASS: kmie_pmx_monitor_base
//
//------------------------------------------------------------------------------

virtual class kmie_pmx_monitor_base #(type T=kmie_pmx_item_in) extends uvm_monitor;

    // The virtual interface to HDL signals.
    virtual kmie_pmx_if m_kmie_pmx_vif;

    // Configuration object
    kmie_pmx_config_obj m_config_obj;

    // Collected item
    T m_collected_item;

    // Collected item is broadcast on this port
    uvm_analysis_port #(T) m_collected_item_port;

    // Broadcasting reset signal
    uvm_analysis_port #(bit) m_reset_port;

//    `uvm_component_utils(kmie_pmx_monitor_base)

    function new (string name, uvm_component parent);
        super.new(name, parent);

        // Allocate collected_item.
        m_collected_item      = T::type_id::create("m_collected_item", this);

        // Allocate collected_item_port.
        m_collected_item_port = new("m_collected_item_port", this);

        // Allocate reset port
        m_reset_port          = new("m_reset_port",this);
    endfunction : new


    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);

        // Get the interface
        if(!uvm_config_db#(virtual kmie_pmx_if)::get(this, "", "m_kmie_pmx_vif", m_kmie_pmx_vif))
            `uvm_fatal("NOVIF", {"virtual interface must be set for: ", get_full_name(), ".m_kmie_pmx_vif"})

        // Get the configuration object
        if(!uvm_config_db#(kmie_pmx_config_obj)::get(this, "", "m_config_obj", m_config_obj))
            `uvm_fatal("NOCONFIG",{"Config object must be set for: ",get_full_name(),".m_config_obj"})
    endfunction: build_phase


    virtual task run_phase(uvm_phase phase);
        process main_thread;    // main thread
        process rst_mon_thread; // reset monitor thread

        // Start monitoring only after an initial reset pulse
        @(negedge m_kmie_pmx_vif.reset) begin
            // Broadcase reset transaction
            m_reset_port.write(1'b0);
            // Wait until reset release
            do @(posedge m_kmie_pmx_vif.clock);
            while(m_kmie_pmx_vif.reset!==1);
            // Broadcase reset transaction
            m_reset_port.write(1'b1);
        end

        // Start monitoring
        forever begin
            fork
                // Start the monitoring thread
                begin
                    main_thread    =process::self();
                    collect_items();
                end
                // Monitor the reset signal
                begin
                    rst_mon_thread = process::self();
                    @(negedge m_kmie_pmx_vif.reset) begin
                        // Interrupt current item at reset
                        if(main_thread) main_thread.kill();
                        // Do reset
                        reset_monitor();
                        // Broadcase reset transaction
                        m_reset_port.write(1'b0);
                    end
                end
            join_any

            if (rst_mon_thread) rst_mon_thread.kill();
        end
    endtask : run_phase

    pure virtual task collect_items();

    pure virtual function void perform_item_checks();

    virtual function void reset_monitor();
    // Reset monitor specific state variables (e.g. counters, flags, buffers, queues, etc.)
    endfunction : reset_monitor

endclass : kmie_pmx_monitor_base

`endif // IFNDEF_GUARD_kmie_pmx_monitor_base
