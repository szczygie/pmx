`include "pixel_matrix_parameters"
`include "column_ctl_params"

`timescale `TIMESCALE

module operation(
//--Inputs--//
	input clk,
	input reset,
	input [`MEMORY_WIDTH-1:0] data_to_pix,
	input [`MEMORY_WIDTH-1:0] data_from_pix,
	input [`MEMORY_DEPTH_WIDTH-1:0] memory_cells,
	input [`PIX_CTL_WIDTH-1:0] control,
	input mem_reg_en,
	input [`MEMORY_OPERATION_CLK_CYCELS_WIDTH-1:0] operation_duration,	//Number of cycles of operation performance
	

//--Outputs--//
	output reg [`PIX_ADDR_WIDTH-1:0] pixel_address,			//Active pixel in column
	output reg [`PIX_CTL_WIDTH-1:0] control_out,			//Pixel control signal
	output reg [`MEMORY_WIDTH-1:0] data_to_pix_out,			//Data sent to pixel
	output reg [`MEMORY_WIDTH-1:0] data_from_pix_out,		//Data from pixel sent from CTL
	output reg pixel_trigger,
	output reg mem_reg_en_out,
	output reg done,
	output reg increment_column_address
	);

	parameter IDLE = 3'b000,
		COLLECT_DATA	   = 3'b100,
		WRITE_MEM          = 3'b001,
		READ_MEM           = 3'b010,
		WRITE_REG          = 3'b101,
		READ_REG           = 3'b110;



//--Next outputs--//
	reg [`PIX_ADDR_WIDTH-1:0] pixel_address_next;
	reg [`PIX_CTL_WIDTH-1:0] control_out_next;
	reg [`MEMORY_WIDTH-1:0] data_to_pix_out_next;
	reg [`MEMORY_WIDTH-1:0] data_from_pix_out_next;
	reg pixel_trigger_next;
	reg mem_reg_en_next;
	reg done_next;
	reg increment_column_address_next;

//--Variables--//
	reg [2:0] state;
	reg [2:0] state_next;

	reg [`MEMORY_DEPTH_WIDTH-1:0] memory_counter;
	reg [`MEMORY_DEPTH_WIDTH-1:0] memory_counter_next;

	reg [`MEMORY_OPERATION_CLK_CYCELS_WIDTH-1:0] memory_operation_counter;
	reg [`MEMORY_OPERATION_CLK_CYCELS_WIDTH-1:0] memory_operation_counter_next;

	reg reset_memory_counter;
	reg reset_memory_counter_next;

	reg increment_pixel_address;
	reg increment_pixel_address_next;

	reg read_last_memory_cell;
	reg read_last_memory_cell_next;

	reg last_cycle;
	reg last_cycle_next;


//--STATE register--//
	always @(posedge clk)begin: State_Register
		if(reset)
			state <= IDLE;
		else
			state <= state_next;
	end

//--Next-state logic--//
	always @* begin: Next_State_Logic
		//default state_next--//
		state_next = IDLE;

		case(state)
			IDLE: begin
				if (control == `PIX_COLLECT_DATA) begin
					state_next = COLLECT_DATA;
				end
				else if(done == 1'b1 || memory_cells == {`MEMORY_DEPTH_WIDTH{1'b0}}) begin
					state_next = IDLE;
				end
				else begin
					case({control, mem_reg_en}) 
						{`PIX_WRITE, 1'b0}: state_next = WRITE_MEM;
						{`PIX_READ, 1'b0}: state_next = READ_MEM;
						{`PIX_READ_AND_RESET, 1'b0}: state_next = READ_MEM;
						{`PIX_WRITE, 1'b1}: state_next = WRITE_REG;
						{`PIX_READ, 1'b1}: state_next = READ_REG;
						{`PIX_READ_AND_RESET, 1'b1}: state_next = READ_REG;
					endcase
				end
			end
			COLLECT_DATA: state_next = (control == `PIX_IDLE) ? IDLE : COLLECT_DATA;
			WRITE_MEM: state_next   = (done == 1'b0) ? WRITE_MEM : IDLE;
			READ_MEM: state_next    = (done == 1'b0) ? READ_MEM : IDLE;
			WRITE_REG: state_next   = (done == 1'b0) ? WRITE_REG : IDLE;
			READ_REG: state_next    = (done == 1'b0) ? READ_REG : IDLE;
		endcase
	end

//--OUTPUT register--//
	always @(posedge clk) begin: Output_Register

//--Outputs--//
		pixel_address            <= pixel_address_next;
		control_out              <= control_out_next;
		data_to_pix_out          <= data_to_pix_out_next;
		data_from_pix_out        <= data_from_pix_out_next;
		pixel_trigger            <= pixel_trigger_next;
		mem_reg_en_out	 	 <= mem_reg_en_next;
		done                     <= done_next;
		increment_column_address <= increment_column_address_next;

//--Variables--//
		memory_counter           <= memory_counter_next;
		memory_operation_counter <= memory_operation_counter_next;
		reset_memory_counter     <= reset_memory_counter_next;
		increment_pixel_address  <= increment_pixel_address_next;
		read_last_memory_cell    <= read_last_memory_cell_next;
		last_cycle		 <= last_cycle_next;
	end

//--Next output logic--/
	always @* begin: Next_Output_Register

//--Default settings--//
		reset_memory_counter_next     = 1'b0;
		increment_pixel_address_next  = 1'b0;
		read_last_memory_cell_next    = 1'b0;
		increment_column_address_next = 1'b0;

		control_out_next 	      = control;
		mem_reg_en_next 	      = state_next[2];

		if(memory_operation_counter == {`MEMORY_OPERATION_CLK_CYCELS_WIDTH{1'b0}})begin
			//--Pixel address logic--//
			if(increment_pixel_address)begin
				pixel_address_next        = pixel_address + 1;
			end
			else begin
				pixel_address_next        = pixel_address;
			end
			//--Memory counter logic--//
			if(reset_memory_counter)begin
				memory_counter_next       = {`MEMORY_DEPTH_WIDTH{1'b0}};
				reset_memory_counter_next = 1'b0;
			end
			else if(read_last_memory_cell) begin
				memory_counter_next       = memory_counter;
			end
			else begin
				memory_counter_next       = memory_counter + 1;
			end


			//--Pixel triggering logic--//
			if(read_last_memory_cell)begin
				pixel_trigger_next        = 1'b0;
			end
			else begin
				pixel_trigger_next        = 1'b1;
			end
		end //end memory_operation_counter == 0
		else begin
			pixel_trigger_next            = 1'b0;
			pixel_address_next            = pixel_address;
			memory_counter_next           = memory_counter;
			last_cycle_next = 1'b0;
		end

		//--Data logic--//
		data_from_pix_out_next        = data_from_pix;
		data_to_pix_out_next          = data_to_pix;

		//--Memory operation counting logic--//
		if(memory_operation_counter == operation_duration-1)begin //Reset to 0 when performed operation_duration cycles to trigger pixel
			memory_operation_counter_next = {`MEMORY_OPERATION_CLK_CYCELS_WIDTH{1'b0}};
		end
		else begin
			memory_operation_counter_next = memory_operation_counter + 1;
		end

		//--Last Operation logic--//
		if(pixel_address == `PIX_IN_COLUMN - 1 && reset_memory_counter) begin
			last_cycle_next = 1'b1;
		end
		else begin
			last_cycle_next = 1'b0;
		end

		//--Done Logic--//
		if(last_cycle) begin
			done_next                     = 1'b1;
		end
		else begin
			done_next                     = 1'b0;
		end

		case(state_next)
			IDLE:begin
				pixel_address_next            = {`PIX_ADDR_WIDTH{1'b0}};
				control_out_next              = `PIX_IDLE;
				data_to_pix_out_next          = {`MEMORY_WIDTH{1'b0}};
				pixel_trigger_next            = 1'b0;
				done_next                     = 1'b0;
				mem_reg_en_next		      = 1'b0;
				memory_counter_next           = {`MEMORY_DEPTH_WIDTH{1'b0}};
				memory_operation_counter_next = {`MEMORY_OPERATION_CLK_CYCELS_WIDTH{1'b0}};
				reset_memory_counter_next     = 1'b1;
				increment_pixel_address_next  = 1'b0;
				read_last_memory_cell_next    = 1'b0;
			end
			COLLECT_DATA: begin
				control_out_next              = `PIX_COLLECT_DATA;
				pixel_address_next            = {`PIX_ADDR_WIDTH{1'b0}};

				data_to_pix_out_next          = {`MEMORY_WIDTH{1'b0}};
				pixel_trigger_next            = 1'b0;
				done_next                     = 1'b0;
				mem_reg_en_next		      = 1'b0;
				memory_counter_next           = {`MEMORY_DEPTH_WIDTH{1'b0}};
				memory_operation_counter_next = {`MEMORY_OPERATION_CLK_CYCELS_WIDTH{1'b0}};
				reset_memory_counter_next     = 1'b1;
				increment_pixel_address_next  = 1'b0;
				read_last_memory_cell_next    = 1'b0;
			end

			WRITE_MEM, WRITE_REG:begin
				
				//--Operation--//
				read_last_memory_cell_next    = 1'b0;
				if(memory_operation_counter == 0) begin
					if(memory_cells == {{`MEMORY_DEPTH_WIDTH-1{1'b0}},{1'b1}})begin //WRITE 1 memory cell
						reset_memory_counter_next    = 1'b1;
						increment_pixel_address_next = 1'b1;
						increment_column_address_next = (pixel_address == `PIX_IN_COLUMN-1) ? 1'b1 : 1'b0;
					end
					else if(memory_cells == {{`MEMORY_DEPTH_WIDTH-2{1'b0}},{1'b1},{1'b0}} && reset_memory_counter)begin //WRITE 2 memory cells
						reset_memory_counter_next    = 1'b0;
						increment_pixel_address_next = 1'b0;
					end
					else if(memory_counter == memory_cells - 2) begin //WRITE any amount of memory cells - {1,2}
						reset_memory_counter_next    = 1'b1;
						increment_pixel_address_next = 1'b1;
						increment_column_address_next = (pixel_address == `PIX_IN_COLUMN-1) ? 1'b1 : 1'b0;
					end
					else begin
						reset_memory_counter_next    = 1'b0;
						increment_pixel_address_next = 1'b0;
					end
				end
				else begin
					reset_memory_counter_next    = reset_memory_counter;
					increment_pixel_address_next = increment_pixel_address;
				end
			end

			READ_MEM, READ_REG:begin

				if(memory_operation_counter == 0)begin
					if(memory_cells == {{`MEMORY_DEPTH_WIDTH-1{1'b0}},{1'b1}})begin //READ 1 memory cell
						read_last_memory_cell_next   = 1'b1;
					end
					else if(memory_cells == {{`MEMORY_DEPTH_WIDTH-2{1'b0}},{1'b1},{1'b0}} && reset_memory_counter)begin //READ 2 memory cells
						read_last_memory_cell_next   = 1'b0;
					end
					else if(memory_counter == memory_cells - 2) begin //READ any amount of memory cells - {1,2}
						read_last_memory_cell_next   = 1'b1;
					end
					else begin
						read_last_memory_cell_next   = 1'b0;
					end
					if(read_last_memory_cell)begin
						reset_memory_counter_next    = 1'b1;
						increment_pixel_address_next = 1'b1;
						increment_column_address_next = (pixel_address == `PIX_IN_COLUMN-1) ? 1'b1 : 1'b0;
						read_last_memory_cell_next   = (pixel_address == `PIX_IN_COLUMN-1) ? 1'b1 : 1'b0;
					end
					else begin
						reset_memory_counter_next    = 1'b0;
						increment_pixel_address_next = 1'b0;
						increment_column_address_next = 1'b0;
					end
				end
				//else if(increment_column_address == 1'b1) begin
				//	read_last_memory_cell_next = 1'b1;	
				//end
				else begin
					reset_memory_counter_next    = reset_memory_counter;
					increment_pixel_address_next = increment_pixel_address;
					read_last_memory_cell_next   = read_last_memory_cell;
				end
			end
		endcase
	end

endmodule
