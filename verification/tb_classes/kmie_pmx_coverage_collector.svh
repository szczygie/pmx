/******************************************************************************
 * DVT CODE TEMPLATE: coverage collector
 * Created by szczygie on Oct 7, 2019
 * uvc_company = coverage, uvc_name = pmx
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_coverage_collector
`define IFNDEF_GUARD_kmie_pmx_coverage_collector

//------------------------------------------------------------------------------
//
// CLASS: kmie_pmx_coverage_collector
//
//------------------------------------------------------------------------------

class kmie_pmx_coverage_collector extends uvm_component;
    `uvm_component_utils(kmie_pmx_coverage_collector)

    localparam REG_MAX = (2**`KMIE_PMX_REG_SIZE)-1;

    // Configuration object
    protected kmie_pmx_config_obj m_config_obj;

    // Item collected from the monitor
    protected kmie_pmx_item_in m_collected_item;

    // Using suffix to handle more ports
    // This defines:
    // umv_analysis_imp_collected_item
    // write_collected_item
    `uvm_analysis_imp_decl(_collected_item)

    // Connection to the monitor
    uvm_analysis_imp_collected_item#(kmie_pmx_item_in, kmie_pmx_coverage_collector) m_monitor_port;

    covergroup item_cg;
        option.per_instance = 1;
        // TODO MTM add coverpoints here
        all_cmd_in_cp : coverpoint m_collected_item.pmx_cmd;

        //------------------------------------------------------------------------------
        // register read/write

        reg_addr_wr: coverpoint m_collected_item.reg_addr iff(m_collected_item.pmx_cmd == PMX_CMD_WR_REG);
        reg_addr_rd: coverpoint m_collected_item.reg_addr iff(m_collected_item.pmx_cmd == PMX_CMD_RD_REG);

        reg_val_wr: coverpoint m_collected_item.reg_val iff(m_collected_item.pmx_cmd == PMX_CMD_WR_REG)
        {
            bins zero   = { '0 };
            bins max    = { REG_MAX } ;
            bins others = { [1:REG_MAX-1] } ;
        }

        reg_wr: cross reg_addr_wr, reg_val_wr;

    //------------------------------------------------------------------------------

    endgroup : item_cg

    function new(string name, uvm_component parent);
        super.new(name, parent);
        item_cg=new;
        item_cg.set_inst_name({get_full_name(), ".item_cg"});
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);

        m_monitor_port = new("m_monitor_port",this);

        // Get the configuration object
        if(!uvm_config_db#(kmie_pmx_config_obj)::get(this, "", "m_config_obj", m_config_obj))
            `uvm_fatal("NOCONFIG", {"Config object must be set for: ", get_full_name(), ".m_config_obj"})
    endfunction : build_phase

    virtual function void report_phase(uvm_phase phase);
        super.report_phase(phase);
        `uvm_info(get_full_name(),
            $sformatf("%s\n--- COVERAGE is %.2f%% ---%s",
                "\n--------------------------",
                item_cg.get_coverage(),
                "\n--------------------------"
            ) ,UVM_LOW)
    endfunction

    function void write_collected_item(kmie_pmx_item_in item);
        m_collected_item = item;
        item_cg.sample();
    endfunction : write_collected_item

endclass : kmie_pmx_coverage_collector

`endif // IFNDEF_GUARD_kmie_pmx_coverage_collector
