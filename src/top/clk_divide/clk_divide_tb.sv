module clk_div_tb();

  logic clk = 0;
  logic rst = 0; // sync reset active high
  wire clk_div;
  wire rst_synced; // reset will be activated after RST_IN_MIN_LEN number of clk cycles

clk_divide
#( 
    .CLK_DIVIDE_BY(4),  
    .RST_IN_MIN_LEN(4),  // minimum number of clk cycles rst should be active to trigger the reset
    .RST_OUT_MIN_LEN(4)  // minimum number of clk_div cycles rst_synced should be active
) u_clk_divide
( .* );

always #5 clk <= ~clk;

initial begin : main
  @(negedge clk) rst <= '1;
  repeat (4) @(negedge clk); rst <= '0;

  repeat(50) @(negedge clk); 

  @(negedge clk) rst <= '1;
  repeat (4) @(negedge clk); rst <= '0;

  repeat(50) @(negedge clk); 
  $finish;
end : main

endmodule
