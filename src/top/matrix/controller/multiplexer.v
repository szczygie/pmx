`include "pixel_matrix_parameters"

`timescale `TIMESCALE

module multiplexer 
	#(
		parameter INPUTS_NR = 2,
		parameter INPUT_WIDTH = 1,
		parameter SELECT_WIDTH = 1
	)

	(
		input [INPUTS_NR*INPUT_WIDTH-1:0] din,
		input [SELECT_WIDTH-1:0] select,
		output [INPUT_WIDTH-1:0] dout

	);

assign dout = (select < INPUTS_NR) ? din[(select+1)*INPUT_WIDTH-1 -: INPUT_WIDTH] : {INPUT_WIDTH{1'b0}}; //dount = 0 if select out of range
//assign dout = din[(select+1)*INPUT_WIDTH-1 -: INPUT_WIDTH]; // dout = X if select out of range

endmodule
