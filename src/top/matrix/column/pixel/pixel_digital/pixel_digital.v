`include "pixel_matrix_parameters"
`timescale `TIMESCALE

module pixel_digital #(
    parameter ADDR              = `PIX_0,
    parameter ADDR_WIDTH        = `PIX_ADDR_WIDTH,
    parameter CTL_WIDTH         = `PIX_CTL_WIDTH,

    parameter MEMORY_ADDR_BITS  = `MEMORY_ADDR_BITS,
    parameter MEMORY_WIDTH      = `MEMORY_WIDTH,
    parameter MEMORY_DEPTH      = `MEMORY_DEPTH,

    parameter CFGREG_REG_NUMBER = `CFGREG_REG_NUMBER,
    parameter CFGREG_WIDTH      = `CFGREG_WIDTH,

    parameter ADC_DATA_WIDTH    = `ADC_DATA_WIDTH,
    parameter ADC_SAMPLE_COUNT  = `ADC_SAMPLE_COUNT
)
(
    input  wire                     clk,             //clk
    input  wire                     rst,             //rst
    input  wire [ADDR_WIDTH-1:0]    addr_in,         //pixel address from matrix_ctl
    input  wire [MEMORY_WIDTH-1:0]  din,             //data to memory or to CfgReg
    input  wire                     pixel_trigger,   //matrix_ctl puts '1' here to indicate that pixel should operate
    input  wire [CTL_WIDTH-1:0]     op_mode,         //operation mode: 00 IDLE, 01 WRITE, 10 READ_AND_RST, 11 READ
    input  wire                     cfgreg_or_ram,   //0-RAM, 1-CfgReg

    output reg  [MEMORY_WIDTH-1:0]  dout,            //data from RAM or CfgReg to matrix_ctl

    input  wire                     DISCRout,        //start ADC conversion
    //ADC_ctl
    input  wire                     comp_result,
    output wire                     start_conv,
    output wire                     clk_comp,
    output wire                     s_sample_bttm,
    output wire                     s_sample_top,
    output wire [ADC_DATA_WIDTH-1:0]s_b5_x,
    output wire [ADC_DATA_WIDTH-1:0]s_bx,
    output wire                     EOC,             //end of conversion
    //cfgreg
    output wire                     EN_KB_PROT,
    output wire                     EN_PDH_Idle_Mode,
    output wire                     PDH_OFF,
    output wire                     VREF_BUF_OFF,
    output wire                     bd_calENABLE,
    output wire [3:0]               bd_gcsa,
    output wire [2:0]               bd_krum,
    output wire [5:0]               bd_THtrim,
    output wire                     RES1,
    output wire                     RES2,
    output wire                     RES3,
    output wire                     RES4,
    output wire                     RES5,
    output wire                     bd_capsw_EN,
    output wire                     PDH_2_ADC_EN,
    output wire                     TEST_ADC,
    output wire                     TEST_V_ADC_VREF
);

//--Internal signals--//
reg                                         op_perform;             //perform pixel operation, commands pixel to operate
reg                                         wrong_addr;             //indicates if addr_in doesn't match pixel address

wire                                        pixel_ctl2ram__read;
wire                                        pixel_ctl2ram__write;
wire [MEMORY_ADDR_BITS-1:0]                 pixel_ctl2ram__addr;
wire [MEMORY_WIDTH-1:0]                     pixel_ctl2ram__ram_din;
wire [MEMORY_WIDTH-1:0]                     ram2pixel_ctl__ram_dout;
wire [MEMORY_WIDTH-1:0]                     pixel_ctl2matrix_ctl__dout;
wire [CFGREG_REG_NUMBER-1:0]                pixel_ctl2cfgreg__en;
wire [CFGREG_WIDTH-1:0]                     pixel_ctl2cfgreg__din;
wire [(CFGREG_REG_NUMBER*CFGREG_WIDTH)-1:0] cfgreg2pixel_ctl__cfg;
wire [ADC_DATA_WIDTH-1:0]                   ADC_ctl2pixel_ctl__conv_result;

//config bits
assign EN_KB_PROT       = cfgreg2pixel_ctl__cfg[0];
assign EN_PDH_Idle_Mode = cfgreg2pixel_ctl__cfg[1];
assign PDH_OFF          = cfgreg2pixel_ctl__cfg[2];
assign VREF_BUF_OFF     = cfgreg2pixel_ctl__cfg[3];
assign bd_calENABLE     = cfgreg2pixel_ctl__cfg[4];
assign bd_gcsa          = cfgreg2pixel_ctl__cfg[8:5];
assign bd_krum          = cfgreg2pixel_ctl__cfg[11:9];
assign bd_THtrim        = cfgreg2pixel_ctl__cfg[17:12];
assign RES1             = cfgreg2pixel_ctl__cfg[18];
assign RES2             = cfgreg2pixel_ctl__cfg[19];
assign RES3             = cfgreg2pixel_ctl__cfg[20];
assign RES4             = cfgreg2pixel_ctl__cfg[21];
assign RES5             = cfgreg2pixel_ctl__cfg[22];
assign bd_capsw_EN      = cfgreg2pixel_ctl__cfg[23];
assign PDH_2_ADC_EN     = cfgreg2pixel_ctl__cfg[24];
assign TEST_ADC         = cfgreg2pixel_ctl__cfg[25];
assign TEST_V_ADC_VREF  = cfgreg2pixel_ctl__cfg[26];

//--Instances--//
pixel_ctl #(
    .CTL_WIDTH(CTL_WIDTH),
    .MEMORY_ADDR_BITS(MEMORY_ADDR_BITS),
    .MEMORY_WIDTH(MEMORY_WIDTH),
    .CFGREG_REG_NUMBER(CFGREG_REG_NUMBER),
    .CFGREG_WIDTH(CFGREG_WIDTH),
    .ADC_DATA_WIDTH(ADC_DATA_WIDTH)
)
u_pixel_ctl(
    .clk (clk ),
    .rst (rst),
    .din (din ),
    .op_mode (op_mode ),
    .op_perform (op_perform ),
    .wrong_addr (wrong_addr ),
    .cfgreg_or_ram (cfgreg_or_ram ),

    .ADC_conv_result (ADC_ctl2pixel_ctl__conv_result ),
    .DISCRout (DISCRout ),
    .EOC (EOC ),
    .ADC_start_conv (start_conv ),

    .ram_dout (ram2pixel_ctl__ram_dout ),
    .ram_read (pixel_ctl2ram__read ),
    .ram_write (pixel_ctl2ram__write ),
    .ram_addr (pixel_ctl2ram__addr ),
    .ram_din (pixel_ctl2ram__ram_din ),

    .cfgreg_cfg (cfgreg2pixel_ctl__cfg ),
    .cfgreg_en (pixel_ctl2cfgreg__en ),
    .cfgreg_din (pixel_ctl2cfgreg__din ),

    .dout2matrix_ctl (pixel_ctl2matrix_ctl__dout )
);


ram64x12 u_ram(
    // Inputs
    .read(pixel_ctl2ram__read),
    .write(pixel_ctl2ram__write),
    .addr(pixel_ctl2ram__addr),
    .din(pixel_ctl2ram__ram_din),
    // Outputs
    .dout(ram2pixel_ctl__ram_dout)
);


cfgreg #(
    .CFGREG_REG_NUMBER(CFGREG_REG_NUMBER),
    .CFGREG_WIDTH(CFGREG_WIDTH)
)
u_cfgreg(
    // Inputs
    .clk(clk),
    .rst(rst),
    .en(pixel_ctl2cfgreg__en),
    .din(pixel_ctl2cfgreg__din),
    // Outputs
    .cfg(cfgreg2pixel_ctl__cfg)
);


ADC_ctl #(
    .SAMPLE_COUNT(ADC_SAMPLE_COUNT),
    .DATA_WIDTH(ADC_DATA_WIDTH)
)
u_ADC_ctl (
    // Inputs
    .clk (clk),
    .rst (rst),
    .start_conv (start_conv),
    .comp_result (comp_result),
    // Outputs
    .s_b5_x (s_b5_x),
    .s_bx (s_bx),
    .s_sample_bttm (s_sample_bttm),
    .s_sample_top (s_sample_top),
    .clk_comp (clk_comp),
    .EOC (EOC),

    .conv_result (ADC_ctl2pixel_ctl__conv_result)
);


//--Decode pixel address--//
always @(posedge clk) begin :addr_decode
    if(rst)begin
        op_perform <= `FALSE;
        wrong_addr <= `FALSE;
    end
    else begin
        if (addr_in == ADDR) begin
            if (pixel_trigger == `TRUE) begin
                op_perform <= `TRUE;
                wrong_addr <= `FALSE;
            end
            else begin
                op_perform <= `FALSE;
                wrong_addr <= `FALSE;
            //empty
            end
        end
        else begin
            op_perform <= `FALSE;
            wrong_addr <= `TRUE;
        end
    end
end
//--Connect instances signals--//
always@* begin
    dout = pixel_ctl2matrix_ctl__dout;
end

endmodule
