/******************************************************************************
 * DVT CODE TEMPLATE: testbench top module
 * Created by szczygie on Oct 7, 2019
 * uvc_company = kmie, uvc_name = pmx
 *******************************************************************************/

module kmie_pmx_tb_top;

// Import the UVM package
import uvm_pkg::*;

// Import the UVC that we have implemented
import kmie_pmx_pkg::*;

// Import all the needed packages


// Clock and reset signals
bit  clock;
bit  reset; // XXX reset is active LOW

// The interface
kmie_pmx_if vif(clock,reset);

// instantiate the DUT

top dut (
    .clk400(clock),
    .rst (reset),
    .sin (vif.sin),
    .sout (vif.sout),
    .b_strobe(1'b0), // ignore this input
    .cfg() // TODO MTM: connect real the DUT
);

initial begin
    // Propagate the interface to all the components that need it
    uvm_config_db#(virtual kmie_pmx_if)::set(uvm_root::get(), "*", "m_kmie_pmx_vif", vif);
    // Start the test
    run_test();
end

// Generate clock
always
    #5 clock=~clock;

// Generate reset
initial begin
    reset     <= '1;
    #21 reset <= 1'b0;
    #51 reset <= 1'b1;
end

//------------------------------------------------------------------------------
// Record the transactions.
//------------------------------------------------------------------------------
// Note: in the base test build phase you need to put:
// uvm_config_db #(int)::set(null, "*", "recording_detail", UVM_FULL);
//------------------------------------------------------------------------------
// Note: this will create *.trn and *.dsn files in the simulation directory
//------------------------------------------------------------------------------
initial $recordvars();

endmodule
