/*
 * Class: ref_model
 * Last modification: 2019-12-23, RSz
 * Description: This is the top class of the PMX chip reference model.
 * 
 * TODO MTM - build the complete reference model with all the necessary classes.
 * Keep OO programming rules (program to interface, one class - one function, etc.)
 */
class ref_model;
    // response of the DUT output queue
    protected kmie_pmx_item_out dut_output_queue[$];

    // process the command and fille the DUT output queue
    // returns the number of items added to the queue
    function int generate_model_response(kmie_pmx_item_in in_item);

        kmie_pmx_item_out out_item;
        out_item          = new();
        out_item.m_data   = in_item.m_data;
        out_item.pmx_cmd  = in_item.pmx_cmd;
        out_item.reg_addr = in_item.reg_addr;
        out_item.reg_val  = in_item.reg_val;

        dut_output_queue.push_back(out_item);

        return 1;
        
    endfunction:generate_model_response


    // retuns the last element from the DUT output queue
    function kmie_pmx_item_out get_item();
        return dut_output_queue.pop_front();
    endfunction:get_item


    function void reset();
        dut_output_queue.delete();
    endfunction:reset

endclass : ref_model