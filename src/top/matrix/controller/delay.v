`include "pixel_matrix_parameters"

`timescale `TIMESCALE

module delay
	#(
		parameter DELAY_NR = 4,
			  SIGNAL_WIDTH = 1
	)
	(	input clk,
		input [SIGNAL_WIDTH-1:0] signal_to_delay,
		output reg [SIGNAL_WIDTH-1:0] delayed_signal
	);

reg [(DELAY_NR-1)*SIGNAL_WIDTH-1:0] delay_bus;

generate
	if(DELAY_NR == 0) begin
		always @* begin
			delayed_signal <= signal_to_delay;
		end
	end
	else begin
		always @(posedge clk) begin
			if(DELAY_NR == 1) begin
				delayed_signal <= signal_to_delay;
			end
			else begin
				{delayed_signal,delay_bus} <= {delay_bus,signal_to_delay};
			end
		end
	end
endgenerate

endmodule
