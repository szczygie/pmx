/******************************************************************************
 * DVT CODE TEMPLATE: scoreboard
 * Created by szczygie on Nov 5, 2019
 * uvc_company = kmie, uvc_name = pmx uvc_if = kmie_pmx_if
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_scoreboard
`define IFNDEF_GUARD_kmie_pmx_scoreboard

//------------------------------------------------------------------------------
//
// CLASS: kmie_pmx_scoreboard
//
//------------------------------------------------------------------------------

class kmie_pmx_scoreboard extends uvm_scoreboard;

    // Configuration object
    protected kmie_pmx_config_obj m_config_obj;

    // TLM analysis FIFOs that fetch data from the monitor
    uvm_tlm_analysis_fifo #(kmie_pmx_item_out) m_in_fifo;
    uvm_tlm_analysis_fifo #(kmie_pmx_item_out) m_out_fifo;

    // Reset TLM FIFO (since this is a transaction level component the reset should be fetched via a TLM analysis FIFO)
    // The reset event can also be fetched in other ways
    uvm_tlm_analysis_fifo #(bit) m_reset_fifo;

    kmie_pmx_item_out scoreboard[$];

    `uvm_component_utils(kmie_pmx_scoreboard)

    function new(string name, uvm_component parent);
        super.new(name, parent);
    endfunction : new

    virtual task scoreboard_match();
        kmie_pmx_item_out in_item;
        kmie_pmx_item_out out_item;
        kmie_pmx_item_out expected_out_item;
        fork
            // Input
            begin
                forever begin
                    // Fetch item from the input interface
                    m_in_fifo.get(in_item);
                    `uvm_info(get_full_name(), $sformatf("\n%s", in_item.sprint()), UVM_HIGH)
                    scoreboard.push_back(in_item);
                end
            end
            // Output
            begin
                forever begin
                    // Fetch item from the output interface
                    m_out_fifo.get(out_item);
                    `uvm_info(get_full_name(), $sformatf("\n%s", out_item.sprint()), UVM_HIGH)
                    // Compare the expected output with the actual output
                    // NOTE: pop_front might not be the best way to get the expected output from the scoreboard queue.
                    // You may want to use your own matching method or the predefined array locator methods.
                    expected_out_item = scoreboard.pop_front();
                    if( ! out_item.compare(expected_out_item) )begin:comp_error
                        `uvm_error("SCOREBOARD_ERROR","Items don't match") // TODO MTM add your own error message
                    end:comp_error
                end
            end
        join
    endtask : scoreboard_match

    // Function that resets the entire scoreboard class to its initial state
    function void reset_scoreboard();
        scoreboard.delete();
        m_in_fifo.flush();
        m_out_fifo.flush();
        m_reset_fifo.flush();
    // Reset other parameters to their initial state
    endfunction : reset_scoreboard

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);

        m_in_fifo    = new("m_in_fifo", this);
        m_out_fifo   = new("m_out_fifo", this);
        m_reset_fifo = new("m_reset_fifo", this);

        // Get the configuration object
        if(!uvm_config_db#(kmie_pmx_config_obj)::get(this, "", "m_config_obj", m_config_obj))
            `uvm_fatal("NOCONFIG", {"Config object must be set for: ", get_full_name(), ".m_config_obj"})
    endfunction : build_phase

    virtual task run_phase(uvm_phase phase);
        process main;// Used by the reset handling mechanism
        bit reset_is_active;
        super.run_phase(phase);

        do m_reset_fifo.get(reset_is_active);
        while(reset_is_active == 1'b0);

        forever begin
            fork
                // Main thread
                begin
                    main=process::self();
                    scoreboard_match();
                end
                // Reset
                begin
                    do m_reset_fifo.get(reset_is_active);
                    while(reset_is_active == 1);

                    main.kill();
                end
            join_any
        end
    endtask : run_phase

endclass : kmie_pmx_scoreboard

`endif // IFNDEF_GUARD_kmie_pmx_scoreboard
