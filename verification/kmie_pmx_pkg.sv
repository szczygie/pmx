/******************************************************************************
 * DVT CODE TEMPLATE: package
 * Created by szczygie on Oct 7, 2019
 * uvc_company = kmie, uvc_name = pmx
 *******************************************************************************/

package kmie_pmx_pkg;

    // UVM macros
    `include "uvm_macros.svh"
    // UVM class library compiled in a package
    import uvm_pkg::*;

    `include "kmie_pmx_macros.svh"

//------------------------------------------------------------------------------
// The command is 16 bit = HEADER + CMD + PAYLOAD
    localparam bit PMX_CMD_IDLE_STATE   = 1'b1;
    localparam bit [1:0] PMX_CMD_HEADER = 2'b01;
    typedef enum bit [1:0] {
        PMX_CMD_1      = 2'b00,
        PMX_CMD_2      = 2'b01,
        PMX_CMD_WR_REG = 2'b10, // register write
        PMX_CMD_RD_REG = 2'b11  // register read
    } pmx_cmd_t;
//------------------------------------------------------------------------------
//  // Configuration object
    `include "kmie_pmx_config_obj.svh"
//  // Sequence item
    `include "kmie_pmx_item_in.svh"
    `include "kmie_pmx_item_out.svh"
//  // Monitors
    `include "kmie_pmx_monitor_base.svh"
    `include "kmie_pmx_monitor_in.svh"
    `include "kmie_pmx_monitor_out.svh"
//  // Coverage Collector
    `include "kmie_pmx_coverage_collector.svh"
//  // Driver
    `include "kmie_pmx_driver.svh"
//  // Sequencer
    `include "kmie_pmx_sequencer.svh"
//  // Scoreboard    
    `include "kmie_pmx_scoreboard.svh"
//  // Agent
    `include "kmie_pmx_agent.svh"
    // Reference model
    `include "ref_model.svh"
    `include "kmie_pmx_ref_model_wrp.svh"
//  // Environment
    `include "kmie_pmx_env.svh"
//  // Sequence library
    `include "kmie_pmx_seq_lib.svh"

//------------------------------------------------------------------------------
// TESTS
    `include "kmie_pmx_base_test.svh"
    `include "kmie_pmx_example_test.svh"
    `include "kmie_pmx_test_init.svh"
    `include "kmie_pmx_test_regs.svh"

endpackage : kmie_pmx_pkg
