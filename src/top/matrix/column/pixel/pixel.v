`include "pixel_matrix_parameters"
`timescale `TIMESCALE

module pixel #(
    parameter ADDR              = `PIX_0,
    parameter ADDR_WIDTH        = `PIX_ADDR_WIDTH,
    parameter CTL_WIDTH         = `PIX_CTL_WIDTH,

    parameter MEMORY_ADDR_BITS  = `MEMORY_ADDR_BITS,
    parameter MEMORY_WIDTH      = `MEMORY_WIDTH,
    parameter MEMORY_DEPTH      = `MEMORY_DEPTH,

    parameter CFGREG_REG_NUMBER = `CFGREG_REG_NUMBER,
    parameter CFGREG_WIDTH      = `CFGREG_WIDTH,

    parameter ADC_DATA_WIDTH    = `ADC_DATA_WIDTH,
    parameter ADC_SAMPLE_COUNT  = `ADC_SAMPLE_COUNT
)
(
    input  wire        clk,           //clk
    input  wire        rst,           //rst
    input  wire [5:0]  addr_in,
    input  wire [11:0] din,
    input  wire [2:0]  op_mode,
    input  wire        pixel_trigger, //matrix_ctl puts '1' here to indicate that pixel should operate
    input  wire        cfgreg_or_ram, //0-RAM, 1-CfgReg
    input  wire        b_strobe,
    output      [11:0] dout
);

// Buses in the design
wire [5:0] s_b5_x;
wire [5:0] s_bx;
wire [3:0] bd_gcsa;
wire [2:0] bd_krum;
wire [5:0] bd_THtrim;

reg        rst_pix;  // reset signal buffered with a single dff

frontend u_frontend (

    .clk_comp (clk_comp ),               //dyn
    .rst ( rst_pix ),                    //dyn
    .s_sample_bttm (s_sample_bttm ),     //dyn
    .s_sample_top (s_sample_top ),       //dyn
    .s_b5_x (s_b5_x ),                   //dyn
    .s_bx (s_bx ),                       //dyn
    .EOC (EOC ),                         //dyn
    .EN_KB_PROT (EN_KB_PROT ),           //stat
    .EN_PDH_Idle_Mode(EN_PDH_Idle_Mode), //stat
    .PDH_OFF (PDH_OFF ),                 //stat
    .VREF_BUF_OFF (VREF_BUF_OFF ),       //stat
    .b_strobe (b_strobe ),               //dyn
    .bd_calENABLE (bd_calENABLE ),       //stat
    .bd_gcsa (bd_gcsa ),                 //stat
    .bd_krum (bd_krum ),                 //stat
    .bd_THtrim (bd_THtrim ),             //stat
    .bd_capsw_EN (bd_capsw_EN ),         //stat
    .PDH_2_ADC_EN (PDH_2_ADC_EN ),       //stat
    .TEST_V_ADC_VREF (TEST_V_ADC_VREF ), //stat

    .comp_result (comp_result ),         //dyn
    .DISCRout (DISCRout ),               //dyn
    .bd_capsw_ctr(EOC)

// FIXME CONNECT FE ANALOG PINS
//    .TEST_ADC (TEST_ADC ),
//    .refVREF_BUF(ana_refVREF_BUF),
//    .CSAout_TEST_GL(ana_CSAout_TEST_GL),
//    .DISCRout_TEST_GL(ana_DISCRout_TEST_GL),
//    .refCSAin_GL(ana_refCSAin_GL),
//    .ctrrefCSAin_GL(ana_ctrrefCSAin_GL),
//    .iPDH_GL(ana_iPDH_GL),
//    .V_ADC_VREF(ana_V_ADC_VREF),
//    .V_ADC_VMID(ana_V_ADC_VMID),
//    .ThN(ana_ThN),
//    .ThP(ana_ThP),
//    .calN(ana_calN),
//    .calP(ana_calP),
//    .iCSAout_GL(ana_iCSAout_GL),
//    .iCSAcas_GL(ana_iCSAcas_GL),
//    .iKRUM_GL(ana_iKRUM_GL),
//    .ctrKRUM_GL(ana_ctrKRUM_GL),
//    .iKRUMrefDAC_GL(ana_iKRUMrefDAC_GL),
//    .iAC_GL(ana_iAC_GL),
//    .refDAC(ana_refDAC),
//    .refDACbase(ana_refDACbase),
//    .iDISCR_GL(ana_iDISCR_GL),
//    .vdda(ana_vdda),
//    .vddd(ana_vddd),
//    .vddm(ana_vddm),
//    .vssa(ana_vssa),
//    .PIXin(ana_PIXin),
//    .vss_pdhadc(ana_vss_pdhadc),
//    .vdd_pdhadc(ana_vdd_pdhadc),
//    .iCSAin_GL(ana_iCSAin_GL),

// for simulation only, not connected
//    .CSAout()
);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
pixel_digital #(
    .ADDR (ADDR ),
    .ADDR_WIDTH (ADDR_WIDTH ),
    .CTL_WIDTH (CTL_WIDTH ),
    .MEMORY_ADDR_BITS (MEMORY_ADDR_BITS ),
    .MEMORY_WIDTH (MEMORY_WIDTH ),
    .MEMORY_DEPTH (MEMORY_DEPTH ),
    .CFGREG_REG_NUMBER(CFGREG_REG_NUMBER),
    .CFGREG_WIDTH (CFGREG_WIDTH ),
    .ADC_DATA_WIDTH (ADC_DATA_WIDTH ),
    .ADC_SAMPLE_COUNT (ADC_SAMPLE_COUNT )
)
u_pixel_digital (
    .clk (clk ),                     //clk
    .rst (rst_pix),
    .addr_in (addr_in ),             //pixel address from matrix_ctl
    .din (din ),                     //data to memory or to CfgReg
    .pixel_trigger (pixel_trigger ), //matrix_ctl puts '1' here to indicate that pixel should operate
    .op_mode (op_mode ),             //operation mode: 00 IDLE, 01 WRITE, 10 READ_AND_RST, 11 READ
    .cfgreg_or_ram (cfgreg_or_ram ), //0-RAM, 1-CfgReg
    .DISCRout (DISCRout ),
    .comp_result (comp_result ),
    .dout (dout ),                   //data from RAM or CfgReg to matrix_ctl
    .start_conv (),
    .clk_comp (clk_comp ),
    .s_sample_bttm (s_sample_bttm ),
    .s_sample_top (s_sample_top ),
    .s_b5_x (s_b5_x ),
    .s_bx (s_bx ),
    .EOC (EOC ),                     //end of conversion
    //cfgreg
    .EN_KB_PROT (EN_KB_PROT ),
    .EN_PDH_Idle_Mode(EN_PDH_Idle_Mode),
    .PDH_OFF (PDH_OFF ),
    .VREF_BUF_OFF (VREF_BUF_OFF ),
    .bd_calENABLE (bd_calENABLE ),
    .bd_gcsa (bd_gcsa ),
    .bd_krum (bd_krum ),
    .bd_THtrim (bd_THtrim ),
    .RES1 (RES1 ),
    .RES2 (RES2 ),
    .RES3 (RES3 ),
    .RES4 (RES4 ),
    .RES5 (RES5 ),
    .bd_capsw_EN (bd_capsw_EN ),
    .PDH_2_ADC_EN (PDH_2_ADC_EN ),
    .TEST_ADC (TEST_ADC ),
    .TEST_V_ADC_VREF (TEST_V_ADC_VREF )
);

// Buffer the reset
always @(posedge clk) rst_pix <= rst;


endmodule
