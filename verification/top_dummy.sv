module top(
    input  wire          clk400,
    input  wire          rst,
    input  wire          sin,
    input  wire          b_strobe, // ignore this input
    output logic [127:0] cfg,
    output logic         sout
);

initial begin
    sout = '1;
    cfg  = '0;
end

// loopback
always @(posedge clk400) begin
    if(rst == 0)begin
        sout <= '1;
    end
    else begin
        sout <= sin;
    end
end

endmodule
