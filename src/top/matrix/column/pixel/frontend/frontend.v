//Verilog HDL for "2019_PX_floorplan", "frontend" "functional"

module frontend( // cadence black_box
    output reg        comp_result,
    input  wire       rst,
    input  wire       clk_comp,
    input  wire       s_sample_bttm,
    input  wire       s_sample_top,
    input  wire [5:0] s_b5_x,
    input  wire [5:0] s_bx,
    input  wire       EOC,
    input  wire       EN_KB_PROT,
    input  wire       EN_PDH_Idle_Mode,
    input  wire       PDH_OFF,
    input  wire       VREF_BUF_OFF,

    input  wire       b_strobe, 
    input  wire       bd_calENABLE,
    input  wire [3:0] bd_gcsa,
    input  wire [2:0] bd_krum,
    input  wire [5:0] bd_THtrim,

    output reg        DISCRout,
    input  wire       bd_capsw_EN,
    input  wire       PDH_2_ADC_EN,
    input  wire       TEST_V_ADC_VREF, // is inout in layout
    input  wire       bd_capsw_ctr

    // below ports routed through all the pixels from the bottom

//    input  wire       TEST_ADC,
//    input  wire       CSAout_TEST_GL,
//    input  wire       DISCRout_TEST_GL,
//    input  wire       V_ADC_VMID,
//    input  wire       V_ADC_VREF,
//    input  wire       refVREF_BUF,
//
//
//    input  wire      PIXin,
//
//    inout  wire      ThN,
//    inout  wire      ThP,
//    inout  wire      calN,
//    inout  wire      calP,
//    inout  wire      ctrKRUM_GL,
//    inout  wire      ctrrefCSAin_GL,
//    inout  wire      iAC_GL,
//    inout  wire      iCSAcas_GL,
//    inout  wire      iCSAin_GL,
//    inout  wire      iCSAout_GL,
//    inout  wire      iDISCR_GL,
//    inout  wire      iKRUM_GL,
//    inout  wire      iKRUMrefDAC_GL,
//    inout  wire      iPDH_GL,
//    inout  wire      refCSAin_GL,
//    inout  wire      refDAC,
//    inout  wire      refDACbase,
//    inout  wire      vdd_pdhadc,
//    inout  wire      vdda,
//    inout  wire      vddd,
//    inout  wire      vddm,
//    inout  wire      vss_pdhadc,
//    inout  wire      vssa,

//    output wire       CSAout           // test output (not connected)
);


`ifndef SYNTHESIS
initial begin : fix_outputs
    comp_result = 1'b0;
    DISCRout    = 1'b0;
end
`endif


endmodule
