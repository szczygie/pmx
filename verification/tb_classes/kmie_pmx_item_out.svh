/******************************************************************************
 * DVT CODE TEMPLATE: sequence item
 * Created by szczygie on Oct 7, 2019
 * uvc_company = kmie, uvc_name = pmx
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_item_out
`define IFNDEF_GUARD_kmie_pmx_item_out

//------------------------------------------------------------------------------
//
// CLASS: kmie_pmx_item_out
//
//------------------------------------------------------------------------------

class kmie_pmx_item_out extends uvm_sequence_item;

    localparam KMIE_REG_ADDR_BITS = $clog2(`KMIE_PMX_REG_COUNT);

    // command content to be send serially to DUT
    bit [`KMIE_PMX_PAYLOAD_LENGTH-1:0] m_data;
    pmx_cmd_t pmx_cmd; // command
    bit [KMIE_REG_ADDR_BITS-1:0] reg_addr;
    bit [`KMIE_PMX_REG_SIZE-1:0] reg_val;

    `uvm_object_utils_begin(kmie_pmx_item_out)
        `uvm_field_int(m_data, UVM_DEFAULT)
        `uvm_field_enum(pmx_cmd_t, pmx_cmd, UVM_ALL_ON)
        `uvm_field_int(reg_addr, UVM_ALL_ON)
        `uvm_field_int(reg_val, UVM_ALL_ON)
    `uvm_object_utils_end

    function new (string name = "kmie_pmx_item_out");
        super.new(name);
    endfunction : new

    //------------------------------------------------------------------------------

    // HINT UVM field macros don't work with unions and structs, you may have to override kmie_pmx_item.do_copy().
    virtual function void do_copy(uvm_object rhs);
        super.do_copy(rhs);
    endfunction : do_copy

    // HINT UVM field macros don't work with unions and structs, you may have to override kmie_pmx_item.do_pack().
    virtual function void do_pack(uvm_packer packer);
        super.do_pack(packer);
    endfunction : do_pack

    // HINT UVM field macros don't work with unions and structs, you may have to override kmie_pmx_item.do_unpack().
    virtual function void do_unpack(uvm_packer packer);
        super.do_unpack(packer);
    endfunction : do_unpack

    // HINT UVM field macros don't work with unions and structs, you may have to override kmie_pmx_item.do_print().
    virtual function void do_print(uvm_printer printer);
        super.do_print(printer);
    endfunction : do_print

    //------------------------------------------------------------------------------
    function bit do_compare (uvm_object rhs, uvm_comparer comparer);
        kmie_pmx_item_out rhs_;
        do_compare = $cast(rhs_,rhs) &&
        super.do_compare(rhs, comparer) &&
        m_data == rhs_.m_data &&
        pmx_cmd == rhs_.pmx_cmd &&
        reg_addr == rhs_.reg_addr &&
        reg_val == rhs_.reg_val;
    endfunction

endclass : kmie_pmx_item_out

`endif // IFNDEF_GUARD_kmie_pmx_item_out
