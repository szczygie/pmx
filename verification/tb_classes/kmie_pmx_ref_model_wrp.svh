/******************************************************************************
 * Reference model wrapper for the PMX chip.
 * Created by szczygie on Dec 19, 2019
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_ref_model_wrp
`define IFNDEF_GUARD_kmie_pmx_ref_model_wrp

//------------------------------------------------------------------------------
//
// CLASS: kmie_pmx_ref_model_wrp
//
//------------------------------------------------------------------------------

class kmie_pmx_ref_model_wrp extends uvm_component;
    `uvm_component_utils(kmie_pmx_ref_model_wrp)

    // TLM analysis FIFOs that fetch data from the monitor
    uvm_tlm_analysis_fifo #(kmie_pmx_item_in) m_in_fifo;

    // Reset TLM FIFO (since this is a transaction level component the reset should be fetched via a TLM analysis FIFO)
    // The reset event can also be fetched in other ways
    uvm_tlm_analysis_fifo #(bit) m_reset_fifo;

    uvm_analysis_port #(kmie_pmx_item_out) m_out_item_ap;

    // reference model
    ref_model model;

    function new(string name, uvm_component parent);
        super.new(name,parent);
        m_out_item_ap = new("m_out_item_ap", this);
        model         = new();
    endfunction : new

    virtual task process_input_data();
        kmie_pmx_item_in in_item;
        kmie_pmx_item_out out_item;
        int dut_response_item_count;
        begin
            forever begin
                // Fetch item from the input interface
                m_in_fifo.get(in_item);
                `uvm_info(get_full_name(), $sformatf("\n%s", in_item.sprint()), UVM_HIGH)

                // send data to DUT model and get the response
                dut_response_item_count = model.generate_model_response(in_item);
                repeat(dut_response_item_count)
                    m_out_item_ap.write(model.get_item());

            end
        end
    endtask : process_input_data

    // Function that resets the entire class to its initial state
    function void reset_ref_model();
        m_in_fifo.flush();
        m_reset_fifo.flush();
        model.reset();
    endfunction : reset_ref_model

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        m_in_fifo    = new("m_in_fifo", this);
        m_reset_fifo = new("m_reset_fifo", this);
    endfunction : build_phase

    virtual task run_phase(uvm_phase phase);
        process main;// Used by the reset handling mechanism
        bit reset_is_active;
        super.run_phase(phase);

        // wait for reset to activate
        do m_reset_fifo.get(reset_is_active);
        while(reset_is_active == 1'b0);

        forever begin
            fork
                // Main thread
                begin
                    main=process::self();
                    process_input_data();
                end
                // Reset
                begin
                    do m_reset_fifo.get(reset_is_active);
                    while(reset_is_active == 1);

                    main.kill();
                end
            join_any
        end
    endtask : run_phase

endclass : kmie_pmx_ref_model_wrp

`endif // IFNDEF_GUARD_kmie_pmx_ref_model_wrp