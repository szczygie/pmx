`timescale 1ns/1ns
`include "column_ctl_params"

module top(
    input  wire                                                        clk400,
    input  wire                                                        rst,
    input  wire                                                        sin,
    input  wire                                                        b_strobe,

    output      [(`GLOBAL_CFGREG_REG_NUMBER*`GLOBAL_CFGREG_WIDTH)-1:0] cfg,
    output wire                                                        sout
);

//--Internal signals--//
wire                          clk100;
wire                          rst_synced;
wire [`COLUMNS_IN_MATRIX-1:0] clk100col; // divided clock for columns

//--Instances--//
clk_divide u_clk_divide(
    // Inputs
    .clk (clk400 ),
    .rst (rst ),
    // Outputs
    .clk_div (clk100 ),
    .clk_div_col(clk100col),
    .rst_synced(rst_synced)
);

matrix u_matrix (
    // Inputs
    .clkx4 (clk400 ),
    .clkx1 (clk100 ),
    .clkx1col (clk100col),
    .sin (sin ),
    .rst (rst_synced),
    .b_strobe(b_strobe),
    // Outputs
    .sout (sout ),
    .cfg(cfg)
);

endmodule
