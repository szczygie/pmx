////////////////////////////////////////////
//		Input data FSM
////////////////////////////////////////////
`include "column_ctl_params"
`include "pixel_matrix_parameters"
`include "serial_transmission_parameters"

`timescale `TIMESCALE

module in_fsm(
	input clk,
	input rst,
	input sin,
	input [`GLOBAL_CFGREG_WIDTH-1:0] dac_reg_in,
	output sout,
	output reg [`MEMORY_WIDTH-1:0] data,
	output reg [`MEMORY_DEPTH_WIDTH+`CONTROL_WIDTH:0] ctl,
	output reg [`GLOBAL_CFGREG_WIDTH+`GLOBAL_CFGREG_EN_WIDTH-1:0] dac_reg_out,
	output reg dac_write,
	output reg dac_read
);

localparam STATE_WIDTH = 2,
//States
	IDLE = 0,
	COUNT_DATA = 1,
	PROBE_DATA = 2;

reg [`SHIFT_REG_WIDTH-1:0] in_data_reg;
reg [`SHIFT_REG_WIDTH-1:0] out_data_reg; // duplicated shift register for data readout
reg [STATE_WIDTH-1:0] state, state_next;
reg [`COUNTER_WIDTH:0] data_count, data_count_next;

reg [`MEMORY_WIDTH-1:0] data_next;
reg [`MEMORY_DEPTH_WIDTH+`CONTROL_WIDTH:0] ctl_next;
reg [`MEMORY_DEPTH_WIDTH+`CONTROL_WIDTH:0] ctl_store;
reg [`MEMORY_DEPTH_WIDTH+`CONTROL_WIDTH:0] ctl_store_next;
reg [`DAC_REG_WIDTH-1:0] dac_reg_out_next;
reg dac_write_next;
reg dac_read_next;


//FSM
always @(posedge clk) begin
	state <= state_next;
	if(rst) begin
		state <= IDLE;
	end
end

always @* begin
	state_next = IDLE;
	case(state)
		IDLE: if(data_count == 0 && ~in_data_reg[`HEADER_WIDTH-1] && in_data_reg[`HEADER_WIDTH-2]) begin
			state_next = COUNT_DATA;
		end
		COUNT_DATA: begin
			state_next = COUNT_DATA;
			if(data_count == `SHIFT_REG_WIDTH-`HEADER_WIDTH)begin
				state_next = PROBE_DATA;
			end
		end
		PROBE_DATA:state_next = IDLE;
	endcase
end

always @(posedge clk) begin
	data <= data_next;
	ctl <= ctl_next;
	ctl_store <= ctl_store_next;
	dac_reg_out <= dac_reg_out_next;
	dac_read <= dac_read_next;
	dac_write <= dac_write_next;
	data_count <= data_count_next;

	if(rst)begin
		data <= {`MEMORY_WIDTH{1'b0}};
		ctl <= {`CONTROL_WIDTH+`MEMORY_DEPTH_WIDTH+1{1'b0}};
		ctl_store <= {`CONTROL_WIDTH+`MEMORY_DEPTH_WIDTH+1{1'b0}};
		dac_reg_out <= {`DAC_REG_WIDTH{1'b0}};
		dac_read <= 1'b0;
		dac_write <= 1'b0;
		data_count <= `HEADER_WIDTH;
	end
end

always @* begin
	data_next = data;//
	//ctl_next = ctl;//
	ctl_next = (ctl[`CONTROL_WIDTH-1:0]==`COLUMN_COLLECT_DATA_COMMAND)? ctl : {`CONTROL_WIDTH{1'b0}};
	ctl_store_next = ctl_store;
	dac_reg_out_next = dac_reg_out;
	data_count_next = `HEADER_WIDTH-1;
	dac_read_next = 1'b0;
	dac_write_next = 1'b0;

	case(state_next)
		IDLE:begin
			data_count_next = data_count == 0 ? 0 : data_count-1;
			if(data_count > 0) begin
				data_next = data;
				ctl_next = ctl;
			end
		end
		COUNT_DATA:begin
			data_count_next = data_count + 1;
		end
		PROBE_DATA:begin
			case(in_data_reg[`SHIFT_REG_WIDTH-1:`SHIFT_REG_WIDTH-`HEADER_WIDTH])
				`CMD_CTL:begin
					data_next = {`MEMORY_WIDTH{1'b0}};//
					ctl_next = in_data_reg[`CONTROL_WIDTH+`MEMORY_DEPTH_WIDTH:0];//
					ctl_store_next = {`CONTROL_WIDTH+`MEMORY_DEPTH_WIDTH+1{1'b0}};//

					case(in_data_reg[`CONTROL_WIDTH-1:0])
						`COLUMN_WRITE_COMMAND,`COLUMN_WRITE_PART_COMMAND:begin
						ctl_next = {`CONTROL_WIDTH+`MEMORY_DEPTH_WIDTH+1{1'b0}};//
						ctl_store_next = in_data_reg[`CONTROL_WIDTH+`MEMORY_DEPTH_WIDTH:0];
						end
					endcase
				end
				`CMD_DATA:begin
					data_next = in_data_reg[`MEMORY_WIDTH-1:0];
					ctl_next = ctl_store;
					ctl_store_next = {`CONTROL_WIDTH+`MEMORY_DEPTH_WIDTH+1{1'b0}};
				end
				`CMD_REG_WRITE:begin
					dac_reg_out_next = in_data_reg[`DAC_REG_WIDTH-1:0];
					dac_write_next = 1'b1;
				end
				`CMD_REG_READ:begin
					data_count_next = `HEADER_WIDTH;
					dac_reg_out_next = {in_data_reg[(`DAC_REG_WIDTH-1)-:`GLOBAL_CFGREG_EN_WIDTH],{`GLOBAL_CFGREG_WIDTH{1'b0}}};
					dac_read_next = 1'b1;
				end
			endcase
		end
	endcase
end


//SHIFT REGISTER CONTROL
//Shift data in
always @(posedge clk) begin
	in_data_reg <= {in_data_reg[`SHIFT_REG_WIDTH-2:0],sin};
	out_data_reg <= {out_data_reg[`SHIFT_REG_WIDTH-2:0],sin};
	if(rst) begin
		in_data_reg <= {`SHIFT_REG_WIDTH{1'b1}};
		out_data_reg <= {`SHIFT_REG_WIDTH{1'b1}};
	end
	else if(dac_read) begin			// Load data in READ CFG state
		out_data_reg <= {`OUT_HEAD_REG,dac_reg_out[`GLOBAL_CFGREG_WIDTH+:`GLOBAL_CFGREG_EN_WIDTH],dac_reg_in};
	end
end

//Shift data out
assign sout = out_data_reg[`SHIFT_REG_WIDTH-1] | dac_read_next;

endmodule
