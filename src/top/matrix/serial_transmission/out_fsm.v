`include "column_ctl_params"
`include "pixel_matrix_parameters"
`include "serial_transmission_parameters"

module out_fsm(
	input clk,
	input reset,
	input [`MEMORY_WIDTH-1:0] data_in,
	input [`CONTROL_WIDTH-1:0] ctl,
	input op_done,
	output reg [`MEMORY_WIDTH+`HEADER_WIDTH-1:0] data_out,
	output reg out_in_sel
);

localparam STATE_WIDTH  = 2;
localparam IDLE = 0,
	   LOAD = 1,
	   SHIFT = 2;

reg [STATE_WIDTH-1:0] state, state_next;
reg [$clog2(`SHIFT_REG_WIDTH)-1:0] shift_counter;
reg [$clog2(`SHIFT_REG_WIDTH)-1:0] shift_counter_next;

reg [`MEMORY_WIDTH+`HEADER_WIDTH-1:0] data_out_next;
reg out_in_sel_next;

always @(posedge clk) begin
	state <= state_next;

	if(reset)begin
		state <= IDLE;
	end
end

always @* begin
	state_next = IDLE;
	case(state)
		IDLE: 	begin
			case(ctl)
				`COLUMN_READ_COMMAND,`COLUMN_READ_AND_RESET_COMMAND,`COLUMN_READ_AND_RESET_PART_COMMAND,`COLUMN_READ_PART_COMMAND: state_next = LOAD;
			endcase
		end
		LOAD: 		state_next = SHIFT;
		SHIFT: 	begin
				state_next = SHIFT;
				if(shift_counter == `SHIFT_REG_WIDTH-1)begin
					state_next = op_done ? IDLE : LOAD;
				end
		end
	endcase
end

always @(posedge clk) begin
	data_out <= data_out_next;
	out_in_sel <= out_in_sel_next;
	shift_counter <= shift_counter_next;

	if(reset)begin
		data_out <= {`MEMORY_WIDTH+`HEADER_WIDTH{1'b0}};
		out_in_sel <= 1'b0;
		shift_counter <= {$clog2(`SHIFT_REG_WIDTH){1'b0}};
	end	
end

always @* begin
	data_out_next = {`MEMORY_WIDTH+`HEADER_WIDTH{1'b1}};
	out_in_sel_next = 1'b0;
	shift_counter_next = {$clog2(`SHIFT_REG_WIDTH){1'b0}};

	case(state_next)
	LOAD: begin
		data_out_next = {`OUT_HEAD_MX,data_in};
		out_in_sel_next = 1'b1;
	end
	SHIFT: begin
		data_out_next = {data_out[`MEMORY_WIDTH+`HEADER_WIDTH-2:0],1'b1};
		shift_counter_next = shift_counter + 1;
		out_in_sel_next = 1'b1;
	end
	endcase
end

endmodule
