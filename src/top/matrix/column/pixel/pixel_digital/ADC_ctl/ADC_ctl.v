`include "pixel_matrix_parameters"
`timescale `TIMESCALE

//Verilog HDL for "SAR_COMPARATOR", "DAC_array_control" "functional" -Dominik Gorni
module ADC_ctl #(
	parameter SAMPLE_COUNT 	= `ADC_SAMPLE_COUNT,
	parameter DATA_WIDTH	= `ADC_DATA_WIDTH
)
( 
	input wire 					clk,
    input wire 					rst,
    input wire					start_conv,
	input wire 					comp_result,
	
	output reg [DATA_WIDTH-1:0] s_b5_x, 		// = 6'b111111,
	output reg [DATA_WIDTH-1:0] s_bx, 			//=  6'b111111,
	output reg 					s_sample_bttm,	// = 1'b1,
	output reg 					s_sample_top,	// = 1'b1,
    output reg 					clk_comp,		// = 1'b0,
	output reg 					EOC,			// = 1'b0,
	
	output reg [DATA_WIDTH-1:0] conv_result		// = 6'b0000000,
);	
//------------------------------------------------------------------------------
// local parameters
//------------------------------------------------------------------------------	
    localparam SAMPLE			= 0;
    localparam TURN_OFF_S_BTTM	= 1;
	localparam SET_B5			= 2;
    localparam TEST_B5			= 3;
	localparam SET_B4			= 4;
	localparam TEST_B4			= 5;
	localparam SET_B3			= 6;
	localparam TEST_B3			= 7;
	localparam SET_B2			= 8;
	localparam TEST_B2			= 9;
	localparam SET_B1			= 10;
    localparam TEST_B1			= 11;
	localparam SET_B0			= 12;
    localparam TEST_B0			= 13;
    localparam RESULT			= 14;
	localparam END_CONV_HOLD	= 15;
	localparam STOP				= 16;
/*
 * INIT ZROBIC.
 * 1. Wszystkie te inicjuajace rzeczy z deklaracji inputow z control_ADC_new3
 * 2. wszystkie inity z local variables tez poinicjalizowac
 */

//------------------------------------------------------------------------------
// local variables
//------------------------------------------------------------------------------
    reg [4:0] state, state_nxt;
    
	reg [DATA_WIDTH-1:0] s_bx_nxt, s_b5_x_nxt;
	reg s_sample_bttm_nxt, s_sample_top_nxt;
	
	reg [2:0] sample_counter, sample_counter_nxt;

	reg clk_comp_nxt;
	reg EOC_nxt;
	reg [5:0] conv_result_nxt;
//------------------------------------------------------------------------------
// state sequential with synchronous reset
//------------------------------------------------------------------------------
    always @(posedge clk) begin : state_seq
	    if (rst) begin
		    state <= STOP;
	    end
		else if (start_conv) begin
            state <= SAMPLE;
        end
        else begin
            state <= state_nxt;
        end
    end
//------------------------------------------------------------------------------
// next state logic
//------------------------------------------------------------------------------
    always @* begin : state_comb
        case(state)
			STOP:
				begin
				state_nxt	=	STOP;
				end
			SAMPLE:
				begin
				state_nxt	=	(sample_counter == SAMPLE_COUNT) ? TURN_OFF_S_BTTM : SAMPLE;
				end
			default:
				begin
				state_nxt = state + 1;
				end
		endcase
    end
//------------------------------------------------------------------------------
// output register
//------------------------------------------------------------------------------
	always @(posedge clk) begin : out_reg
        if (rst) begin : out_reg_rst
			s_bx			<= 6'b111111;
			s_b5_x			<= 6'b111111;
			s_sample_bttm	<= 1'b1;
			s_sample_top	<= 1'b1;
			clk_comp		<= 1'b0;
			EOC				<= 1'b0;
			conv_result		<= 6'b000000;
			sample_counter	<= 3'b000;
        end
        else begin : out_reg_run
			s_bx			<= s_bx_nxt;
			s_b5_x 			<= s_b5_x_nxt;
			s_sample_bttm	<= s_sample_bttm_nxt;
			s_sample_top	<= s_sample_top_nxt;
			clk_comp		<= clk_comp_nxt;
			EOC				<= EOC_nxt;
			conv_result		<= conv_result_nxt;
			sample_counter	<= sample_counter_nxt;
        end
    end
//------------------------------------------------------------------------------
// output logic
//------------------------------------------------------------------------------
    always @* begin : out_comb
		clk_comp_nxt 		= ~clk_comp; 
		EOC_nxt				= 1'b0;
	    s_sample_bttm_nxt	= 1'b0;
		s_sample_top_nxt 	= 1'b0;
	    conv_result_nxt		= 6'b000000;
	    sample_counter_nxt	= 3'b000;
        case(state_nxt)
            SAMPLE:           
                begin
				s_bx_nxt			= 6'b111111;
				s_b5_x_nxt 			= 6'b111111;
				s_sample_bttm_nxt	= 1'b1;
				s_sample_top_nxt	= 1'b1;
				sample_counter_nxt	= sample_counter + 1'b1;
				clk_comp_nxt		= 1'b0;
                end 			
			TURN_OFF_S_BTTM:           
                begin
				s_bx_nxt			= 6'b000000;
				s_b5_x_nxt 			= 6'b111111;
				s_sample_top_nxt	= 1'b1;
                end			
			SET_B5:           
                begin
				s_bx_nxt			= 6'b000000;
				s_b5_x_nxt 			= 6'b111111;
                end
			TEST_B5:           
                begin
				s_bx_nxt			= 6'b000000;
				s_b5_x_nxt 			= 6'b111111;
				conv_result_nxt		= {~comp_result, 5'b00000};
                end
			SET_B4:           
				begin
				if (comp_result == 1'b0)
					begin
					s_b5_x_nxt 		= 6'b111111;
					s_bx_nxt 		= 6'b100000;
					end
				else
					begin
					s_b5_x_nxt 		= 6'b011111;
					s_bx_nxt 		= 6'b000000;
					end
				conv_result_nxt		= {~comp_result, 5'b00000};
				end
			TEST_B4:           
                begin
				s_bx_nxt			= s_bx;
				s_b5_x_nxt 			= s_b5_x;
				conv_result_nxt		= {conv_result[5], ~comp_result, 4'b0000};
                end	
			SET_B3:           
                begin
				if (comp_result == 1'b0)
					begin
					s_b5_x_nxt 		= {s_b5_x[5], 5'b11111};
					s_bx_nxt 		= {s_bx[5], 5'b10000};
					end
				else
					begin
					s_b5_x_nxt 		= {s_b5_x[5], 5'b01111};
					s_bx_nxt 		= {s_bx[5], 5'b00000};
					end
				conv_result_nxt		= {conv_result[5], ~comp_result, 4'b0000};
				end
			TEST_B3:           
                begin
				s_bx_nxt			= s_bx;
				s_b5_x_nxt 			= s_b5_x;
				conv_result_nxt		= {conv_result[5:4], ~comp_result, 3'b000};
                end	
			SET_B2:           
                begin
				if (comp_result == 1'b0)
					begin
					s_b5_x_nxt 		= {s_b5_x[5:4], 4'b1111};
					s_bx_nxt 		= {s_bx[5:4], 4'b1000};
					end
				else
					begin
					s_b5_x_nxt 		= {s_b5_x[5:4], 4'b0111};
					s_bx_nxt 		= {s_bx[5:4], 4'b0000};
					end
				conv_result_nxt		= {conv_result[5:4], ~comp_result, 3'b000};
				end	
			TEST_B2:           
                begin
				s_bx_nxt			= s_bx;
				s_b5_x_nxt 			= s_b5_x;
				conv_result_nxt		= {conv_result[5:3], ~comp_result, 2'b00};
                end	
			SET_B1:           
                begin
				if (comp_result == 1'b0)
					begin
					s_b5_x_nxt 		= {s_b5_x[5:3], 3'b111};
					s_bx_nxt 		= {s_bx[5:3], 3'b100};
					end
				else
					begin
					s_b5_x_nxt 		= {s_b5_x[5:3], 3'b011};
					s_bx_nxt 		= {s_bx[5:3], 3'b000};
					end
				conv_result_nxt		= {conv_result[5:3], ~comp_result, 2'b00};
				end	
			TEST_B1:           
                begin
				s_bx_nxt			= s_bx;
				s_b5_x_nxt 			= s_b5_x;
				conv_result_nxt		= {conv_result[5:2], ~comp_result, 1'b0};
                end
			SET_B0:           
                begin
				if (comp_result == 1'b0)
					begin
					s_b5_x_nxt 		= {s_b5_x[5:2], 2'b11};
					s_bx_nxt 		= {s_bx[5:2], 2'b10};
					end
				else
					begin
					s_b5_x_nxt 		= {s_b5_x[5:2], 2'b01};
					s_bx_nxt 		= {s_bx[5:2], 2'b00};
					end
				conv_result_nxt		= {conv_result[5:2], ~comp_result, 1'b0};
				end	
			TEST_B0:           
                begin
				s_bx_nxt			= s_bx;
				s_b5_x_nxt 			= s_b5_x;
				conv_result_nxt		= {conv_result[5:1], ~comp_result};
                end
			RESULT:           
                begin
				s_bx_nxt			= s_bx;
				s_b5_x_nxt 			= s_b5_x;
				EOC_nxt		 		= 1'b1;
				conv_result_nxt		= {conv_result[5:1], ~comp_result};
                end
			END_CONV_HOLD:           
                begin
				s_bx_nxt			= 6'b111111;
				s_b5_x_nxt 			= 6'b111111;
				s_sample_bttm_nxt	= 1'b1;
				s_sample_top_nxt 	= 1'b1;
				EOC_nxt		 		= 1'b1;
				conv_result_nxt		= conv_result;
				clk_comp_nxt		= 1'b0;
                end
			STOP:           
                begin
				s_bx_nxt			= 6'b111111;
				s_b5_x_nxt 			= 6'b111111;
				s_sample_bttm_nxt	= 1'b1;
				s_sample_top_nxt 	= 1'b1;
				EOC_nxt		 		= 1'b0;
				conv_result_nxt		= conv_result;
				clk_comp_nxt		= 1'b0;
                end
			default:           
				begin
				s_bx_nxt			= 6'b000000;
				s_b5_x_nxt			= 6'b000000;
				s_sample_top_nxt	= 1'b1;
				end
        endcase
    end 
endmodule
