/******************************************************************************
 * DVT CODE TEMPLATE: sequence item
 * Created by szczygie on Oct 7, 2019
 * uvc_company = kmie, uvc_name = pmx
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_item_in
`define IFNDEF_GUARD_kmie_pmx_item_in

//------------------------------------------------------------------------------
//
// CLASS: kmie_pmx_item_in
//
//------------------------------------------------------------------------------
// TODO MTM: replace the contents of this file
//------------------------------------------------------------------------------

class kmie_pmx_item_in extends uvm_sequence_item;

    localparam KMIE_REG_ADDR_BITS = $clog2(`KMIE_PMX_REG_COUNT);

    // TODO MMM Declare fields here

    // command content to be send serially to DUT
    rand bit [`KMIE_PMX_PAYLOAD_LENGTH-1:0] m_data;
    rand pmx_cmd_t pmx_cmd; // command
    rand bit [KMIE_REG_ADDR_BITS-1:0] reg_addr;
    rand bit [`KMIE_PMX_REG_SIZE-1:0] reg_val;

    `uvm_object_utils_begin(kmie_pmx_item_in)
        `uvm_field_int(m_data, UVM_DEFAULT)
        `uvm_field_enum(pmx_cmd_t, pmx_cmd, UVM_ALL_ON)
        `uvm_field_int(reg_addr, UVM_ALL_ON)
        `uvm_field_int(reg_val, UVM_ALL_ON)
    `uvm_object_utils_end

    function new (string name = "kmie_pmx_item_in");
        super.new(name);
    endfunction : new
    
    //------------------------------------------------------------------------------
    constraint c_reg_write {
        pmx_cmd == PMX_CMD_WR_REG -> m_data == {reg_addr, reg_val };
    }
    //------------------------------------------------------------------------------
    constraint c_reg_read {
        pmx_cmd == PMX_CMD_RD_REG -> m_data == {reg_addr, reg_val };
    }
    //------------------------------------------------------------------------------

    // HINT UVM field macros don't work with unions and structs, you may have to override kmie_pmx_item.do_copy().
    virtual function void do_copy(uvm_object rhs);
        super.do_copy(rhs);
    endfunction : do_copy

    // HINT UVM field macros don't work with unions and structs, you may have to override kmie_pmx_item.do_pack().
    virtual function void do_pack(uvm_packer packer);
        super.do_pack(packer);
    endfunction : do_pack

    // HINT UVM field macros don't work with unions and structs, you may have to override kmie_pmx_item.do_unpack().
    virtual function void do_unpack(uvm_packer packer);
        super.do_unpack(packer);
    endfunction : do_unpack

    // HINT UVM field macros don't work with unions and structs, you may have to override kmie_pmx_item.do_print().
    virtual function void do_print(uvm_printer printer);
        super.do_print(printer);
    endfunction : do_print

endclass : kmie_pmx_item_in

`endif // IFNDEF_GUARD_kmie_pmx_item_in
