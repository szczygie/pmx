/******************************************************************************
 * DVT CODE TEMPLATE: base test
 * Created by szczygie on Oct 8, 2019
 * uvc_company = kmie, uvc_name = pmx
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_base_test
`define IFNDEF_GUARD_kmie_pmx_base_test

class kmie_pmx_base_test extends uvm_test;

    // Env instance
    kmie_pmx_env m_env;

    // Table printer instance(optional)
    uvm_table_printer printer;

// The class is never instantiated, do not need `uvm_component_utils
//    `uvm_component_utils(kmie_pmx_base_test)

    function new(string name = "kmie_pmx_base_test", uvm_component parent=null);
        super.new(name,parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);

        // Create the env
        m_env               = kmie_pmx_env::type_id::create("m_env", this);

        // Create a specific depth printer for printing the created topology
        printer             = new();
        printer.knobs.depth = 3;

        // record all transactions
        // note: enable transaction recording in xcelium with $recordvars() 
        //       in the testbench top module
        uvm_config_db #(int)::set(null, "*", "recording_detail", UVM_FULL);
    endfunction : build_phase

    virtual function void end_of_elaboration_phase(uvm_phase phase);
        // Print the test topology
        `uvm_info(get_type_name(), $sformatf("Printing the test topology :\n%s", this.sprint(printer)), UVM_LOW)
    endfunction : end_of_elaboration_phase

    task run_phase(uvm_phase phase);
        super.run_phase(phase);
    // HINT Here you can set the drain-time if desired
    // phase.phase_done.set_drain_time(this, 10);
    endtask : run_phase

endclass : kmie_pmx_base_test

`endif // IFNDEF_GUARD_kmie_pmx_base_test
