`include "pixel_matrix_parameters"
`timescale `TIMESCALE

module cfgreg #(
		parameter CFGREG_REG_NUMBER	= `CFGREG_REG_NUMBER,
		parameter CFGREG_WIDTH 		= `CFGREG_WIDTH
	)
	(
		input wire 											clk,
		input wire											rst,
		input wire [CFGREG_REG_NUMBER-1:0]					en,		//clock enable
		input wire [CFGREG_WIDTH-1:0]						din,
		
		output reg [(CFGREG_REG_NUMBER*CFGREG_WIDTH)-1:0]	cfg
		//output reg [CFGREG_WIDTH-1:0]		dout
		);
	
	//--Write to CfgReg--//
	always @(posedge clk) begin
		if (rst) begin
			cfg <= {(CFGREG_REG_NUMBER*CFGREG_WIDTH){1'b0}};
		end
		else begin
			case(en)
					4'b0001: cfg[(1*CFGREG_WIDTH)-1:(0*CFGREG_WIDTH)]	<= din;
					4'b0010: cfg[(2*CFGREG_WIDTH)-1:(1*CFGREG_WIDTH)] 	<= din;
					4'b0100: cfg[(3*CFGREG_WIDTH)-1:(2*CFGREG_WIDTH)] 	<= din;
					4'b1000: cfg[(4*CFGREG_WIDTH)-1:(3*CFGREG_WIDTH)] 	<= din;
					default: cfg										<= cfg;
			endcase
		end
	end

endmodule
