/******************************************************************************
 * Created by szczygie on Oct 16, 2019
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_test_regs
`define IFNDEF_GUARD_kmie_pmx_test_regs

class kmie_pmx_test_regs extends kmie_pmx_base_test;

    `uvm_component_utils(kmie_pmx_test_regs)


    function new(string name = "kmie_pmx_test_regs", uvm_component parent);
        super.new(name, parent);
    endfunction: new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
    endfunction

    virtual task run_phase(uvm_phase phase);
        kmie_pmx_sequence_regs seq_regs;
        super.run_phase(phase);
        
        seq_regs = kmie_pmx_sequence_regs::type_id::create("seq_regs");
        
        // needed for setting objections in the base_sequence
        seq_regs.set_starting_phase(phase);
        
        seq_regs.start(m_env.m_kmie_pmx_agent.m_sequencer);
    endtask

endclass

`endif // IFNDEF_GUARD_kmie_pmx_test_regs
