//***************************************//
// 	 Shift Register Controller
//**************************************//
`timescale 1ns/1ns
`include "column_ctl_params"
`include "pixel_matrix_parameters"

module shift_register_ctl
#(parameter SHIFT_REG_WIDTH = 16,
  parameter HEADER = 4'b1010,
  parameter HEADER_WIDTH = 4,
  parameter PIX_MEMORY_WIDTH = 12,
  parameter PIX_CONTROL_WIDTH = 3,
  parameter PIX_MEMORY_DEPTH_WIDTH = 7)(

	input clk,
	input reset,
	
	//From SR to pixel
	input [SHIFT_REG_WIDTH-1:0] sr_pin,
	output reg [SHIFT_REG_WIDTH-1:0] sr_pout,
	output reg sr_pwrite,

	//From pixel to SR
	input pix_operation_finished,
	input [PIX_MEMORY_WIDTH-1:0] pix_data_from_pix,
	output reg [PIX_MEMORY_WIDTH-1:0] pix_data_to_pix,
	output reg [PIX_CONTROL_WIDTH-1:0] pix_control,
	output reg pix_mem_reg_en,
	output reg [PIX_MEMORY_DEPTH_WIDTH-1:0] pix_number_of_mem
);
localparam STATE_WIDTH = 3;
localparam RESET = {STATE_WIDTH{1'b0}},
	GET_CONTROL = {{STATE_WIDTH-1{1'b0}},1'b1},
	WRITE_TO_PIX = {{STATE_WIDTH-2{1'b0}},2'b10},
	READ_FROM_PIX = {{STATE_WIDTH-2{1'b0}},2'b11},
	COLLECT_DATA = {{STATE_WIDTH-2{1'b0}},3'b100};
	
reg [STATE_WIDTH-1:0] state, state_next;
//Next outputs
reg [SHIFT_REG_WIDTH-1:0] sr_pout_next;
reg sr_pwrite_next;
reg [PIX_MEMORY_WIDTH-1:0] pix_data_to_pix_next;
reg [PIX_CONTROL_WIDTH-1:0] pix_control_next;
reg pix_mem_reg_en_next;
reg [PIX_MEMORY_DEPTH_WIDTH-1:0] pix_number_of_mem_next;

reg [PIX_CONTROL_WIDTH-1:0] control_del_next;
reg [PIX_CONTROL_WIDTH-1:0] control_del;

// State logic
always @(posedge clk) begin
	state <= state_next;
	if(reset) begin
		state <= RESET;
	end
end

// Next state logic
always @* begin
	state_next = RESET;
	case(state)
	RESET: 	if(sr_pin[SHIFT_REG_WIDTH-1:SHIFT_REG_WIDTH-HEADER_WIDTH] == HEADER && sr_pin[PIX_CONTROL_WIDTH-1:0]!={PIX_CONTROL_WIDTH{1'b0}})begin
	       		state_next = GET_CONTROL;
	       	end
	GET_CONTROL: 	case(control_del)
			`COLUMN_WRITE_COMMAND, 
			`COLUMN_WRITE_PART_COMMAND:	state_next = WRITE_TO_PIX;	
			`COLUMN_READ_COMMAND, 
			`COLUMN_READ_AND_RESET_COMMAND, 
			`COLUMN_READ_AND_RESET_PART_COMMAND, 
			`COLUMN_READ_PART_COMMAND: 	state_next = READ_FROM_PIX;
			`COLUMN_COLLECT_DATA_COMMAND: 	state_next = COLLECT_DATA;
			endcase
	WRITE_TO_PIX:	begin
			state_next = WRITE_TO_PIX;
			if(pix_operation_finished)begin
				state_next = RESET;	
			end
		end
	READ_FROM_PIX:	begin
			state_next = READ_FROM_PIX;
			if(pix_operation_finished)begin
				state_next = RESET;	
			end
		end
	COLLECT_DATA:begin
			state_next = COLLECT_DATA;
			if(sr_pin == {HEADER,{SHIFT_REG_WIDTH-HEADER_WIDTH{1'b0}}})begin
				state_next = RESET;
			end
		end
	endcase
end

// Output logic
always @(posedge clk) begin
	sr_pout <= sr_pout_next;
	sr_pwrite <= sr_pwrite_next;
	pix_data_to_pix <= pix_data_to_pix_next;
	pix_control <= pix_control_next;
	pix_mem_reg_en <= pix_mem_reg_en_next;
	pix_number_of_mem <= pix_number_of_mem_next;
	//Variable
	control_del <= control_del_next;
end

always @* begin
	sr_pout_next = sr_pout;
	sr_pwrite_next = sr_pwrite;
	pix_data_to_pix_next = pix_data_to_pix;
	pix_control_next = pix_control;
	pix_mem_reg_en_next = pix_mem_reg_en;
	pix_number_of_mem_next = pix_number_of_mem;
	control_del_next = control_del;

	case(state_next)
	RESET:		begin
				sr_pout_next = 0;
				sr_pwrite_next = 0;
				pix_data_to_pix_next = 0;
				pix_control_next = 0;
				pix_mem_reg_en_next = 0;
				pix_number_of_mem_next = 0;
				control_del_next = 0;
			end
	READ_FROM_PIX:		begin
				pix_control_next = control_del;
				sr_pout_next = {{HEADER},pix_data_from_pix};
			end
	WRITE_TO_PIX:		begin
				pix_control_next = control_del;
				pix_data_to_pix_next = sr_pin[PIX_MEMORY_WIDTH-1:0];
			end
	GET_CONTROL:	begin
				control_del_next = sr_pin[PIX_CONTROL_WIDTH-1:0];
			end
	COLLECT_DATA:	begin
				pix_control_next = control_del;
			end
	endcase
end

endmodule
