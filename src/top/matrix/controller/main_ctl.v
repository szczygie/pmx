`include "pixel_matrix_parameters"
`include "column_ctl_params"

`timescale `TIMESCALE

module main_ctl
	#(
	parameter COLUMN_RESET_STATE	 = 4'b0000,
	COLUMN_WRITE_STATE               = 4'b0001,
	COLUMN_READ_STATE                = 4'b0011,
	COLUMN_READ_AND_RESET_STATE      = 4'b0010,
	COLUMN_READ_AND_RESET_PART_STATE = 4'b0110,
	COLUMN_READ_PART_STATE           = 4'b0111,
	COLUMN_WRITE_PART_STATE          = 4'b0101,
	COLUMN_COLLECT_DATA_STATE        = 4'b0100,
	INCREMENT_COLUMN_ADDR		 = 4'b1000,
	FINISH_STATE 			 = 4'b1001
	)
	(
	input clk,
	input reset,
	input [`MEMORY_WIDTH-1:0] data_to_pix_in,
	input [`MEMORY_WIDTH-1:0] data_from_pix_in,
	input [`CONTROL_WIDTH-1:0] control_in,
	input [`MEMORY_DEPTH_WIDTH-1:0] number_of_mem_in,
	input mem_reg_en_in,
	input done,

	output reg [`MEMORY_WIDTH-1:0] data_to_pix_out,
	output reg [`MEMORY_WIDTH-1:0] data_from_pix_out,
	output reg [`CONTROL_WIDTH-1:0] control_out,
	output reg [`COLUMN_ADDR_WIDTH-1:0] column_addr_out,
	output reg [`MEMORY_DEPTH_WIDTH-1:0] number_of_mem_out,
	output reg mem_reg_en_out,
	output reg state_finished
	);

localparam STATE_REG_WIDTH = 4;

reg [STATE_REG_WIDTH-1:0] state;
reg [STATE_REG_WIDTH-1:0] state_next;

reg [STATE_REG_WIDTH-1:0] operation; //Added to retur to proper state after column address increment
reg [STATE_REG_WIDTH-1:0] operation_next;


reg [`MEMORY_WIDTH-1:0] data_to_pix_next;
reg [`MEMORY_WIDTH-1:0] data_from_pix_next;
reg [`CONTROL_WIDTH-1:0] control_next;
reg [`COLUMN_ADDR_WIDTH-1:0] column_addr_next;
reg [`MEMORY_DEPTH_WIDTH-1:0] number_of_mem_next;
reg mem_reg_en_next;
reg state_finished_next; 
reg start, start_next;

wire finish_operation;

delay #(
	.DELAY_NR    (3),
	.SIGNAL_WIDTH(1)
)
done_op_delay (
	.clk            (clk),
	.delayed_signal (finish_operation),
	.signal_to_delay(done)
);


always @(posedge clk) begin: State_Register
    if(reset)
	state <= COLUMN_RESET_STATE;
    else
	state <= state_next;
end

always @* begin: Next_State_Logic
	
	state_next = COLUMN_RESET_STATE;
	
	case(state)
	COLUMN_RESET_STATE: begin
		case(control_in)
		`COLUMN_COLLECT_DATA_COMMAND:   	state_next = COLUMN_COLLECT_DATA_STATE;
		`COLUMN_WRITE_COMMAND:	        	state_next = COLUMN_WRITE_STATE;
		`COLUMN_READ_COMMAND:	        	state_next = COLUMN_READ_STATE;
		`COLUMN_READ_AND_RESET_COMMAND: 	state_next = COLUMN_READ_AND_RESET_STATE; 
		`COLUMN_WRITE_PART_COMMAND:		state_next = COLUMN_WRITE_PART_STATE;
		`COLUMN_READ_PART_COMMAND:		state_next = COLUMN_READ_PART_STATE;
		`COLUMN_READ_AND_RESET_PART_COMMAND: 	state_next = COLUMN_READ_AND_RESET_PART_STATE; 
		endcase	
	end
	COLUMN_COLLECT_DATA_STATE: state_next = (control_in == `COLUMN_RESET_COMMAND) ? FINISH_STATE : COLUMN_COLLECT_DATA_STATE;

	COLUMN_WRITE_STATE,
	COLUMN_WRITE_PART_STATE:
		state_next = done ? (column_addr_out == `COLUMNS_IN_MATRIX-1) ? FINISH_STATE : INCREMENT_COLUMN_ADDR : state;

	COLUMN_READ_STATE,
	COLUMN_READ_AND_RESET_STATE,
	COLUMN_READ_PART_STATE,
	COLUMN_READ_AND_RESET_PART_STATE: begin
		state_next = state;

		if(done) begin
			state_next = INCREMENT_COLUMN_ADDR;
		end
		if(column_addr_out == `COLUMNS_IN_MATRIX && finish_operation) begin
			state_next = FINISH_STATE;
		end
	end
	INCREMENT_COLUMN_ADDR: state_next = operation;
	FINISH_STATE: state_next = COLUMN_RESET_STATE;
	endcase
end

always @(posedge clk) begin: Output_Register
	//Output
	data_to_pix_out <= data_to_pix_next;
	data_from_pix_out <= data_from_pix_next;
	control_out <= control_next;
	column_addr_out <= column_addr_next;
	number_of_mem_out <= number_of_mem_next;
	mem_reg_en_out <= mem_reg_en_next;
	state_finished <= state_finished_next;
	start <= start_next;

	//Variable
	operation <= operation_next;
	if(reset)begin
		data_to_pix_out <= {`MEMORY_WIDTH{1'b0}};
		data_from_pix_out <= {`MEMORY_WIDTH{1'b0}};
		control_out <= {`CONTROL_WIDTH{1'b0}};
		column_addr_out <= {`COLUMN_ADDR_WIDTH{1'b0}};
		number_of_mem_out <= {`MEMORY_DEPTH_WIDTH{1'b0}};
		mem_reg_en_out <= 1'b0;
		state_finished <= 1'b0;
		operation <= COLUMN_RESET_STATE;
		start <= 1'b0;
	end
end

always @* begin: Next_Output_Logic
	//--Default settings--//
	data_to_pix_next = {`MEMORY_WIDTH{1'b0}};
	data_from_pix_next = {`MEMORY_WIDTH{1'b0}};
	control_next = {`CONTROL_WIDTH{1'b0}};
	column_addr_next = {`COLUMN_ADDR_WIDTH{1'b0}};
	number_of_mem_next = {`MEMORY_DEPTH_WIDTH{1'b0}};
	mem_reg_en_next = 1'b0;
	operation_next = state_next;
	state_finished_next = 1'b0;
	start_next = 1'b1;

	case(state_next)
	COLUMN_WRITE_STATE:
	begin
		data_to_pix_next = data_to_pix_in;
		control_next = `COLUMN_WRITE_COMMAND;
		column_addr_next = column_addr_out;
		number_of_mem_next = start ? number_of_mem_out : number_of_mem_in;
		mem_reg_en_next = start ? mem_reg_en_out : mem_reg_en_in;
	end

	COLUMN_READ_STATE:
	begin
		data_from_pix_next = data_from_pix_in;
		control_next = `COLUMN_READ_COMMAND;
		column_addr_next = column_addr_out;
		number_of_mem_next = start ? number_of_mem_out : number_of_mem_in;
		mem_reg_en_next = start ? mem_reg_en_out : mem_reg_en_in;
	end

	COLUMN_READ_AND_RESET_STATE:
	begin
		data_from_pix_next = data_from_pix_in;
		control_next = `COLUMN_READ_AND_RESET_COMMAND;
		column_addr_next = column_addr_out;
		number_of_mem_next = start ? number_of_mem_out : number_of_mem_in;
		mem_reg_en_next = start ? mem_reg_en_out : mem_reg_en_in;
	end

	COLUMN_WRITE_PART_STATE:
	begin
		data_to_pix_next = data_to_pix_in;
		control_next = `COLUMN_WRITE_PART_COMMAND;
		column_addr_next = column_addr_out;
		number_of_mem_next = start ? number_of_mem_out : number_of_mem_in;
		mem_reg_en_next = start ? mem_reg_en_out : mem_reg_en_in;
	end

	COLUMN_READ_PART_STATE:
	begin
		data_from_pix_next = data_from_pix_in;
		control_next = `COLUMN_READ_PART_COMMAND;
		column_addr_next = column_addr_out;
		number_of_mem_next = start ? number_of_mem_out : number_of_mem_in;
		mem_reg_en_next = start ? mem_reg_en_out : mem_reg_en_in;
	end


	COLUMN_READ_AND_RESET_PART_STATE: 
	begin
		data_from_pix_next = data_from_pix_in;
		control_next = `COLUMN_READ_AND_RESET_PART_COMMAND;
		column_addr_next = column_addr_out;
		number_of_mem_next = start ? number_of_mem_out : number_of_mem_in;
		mem_reg_en_next = start ? mem_reg_en_out : mem_reg_en_in;
	end

	INCREMENT_COLUMN_ADDR: begin
		data_to_pix_next = data_to_pix_out;
		data_from_pix_next = data_from_pix_in;
		control_next = control_out;
		column_addr_next = column_addr_out + 1;
		number_of_mem_next = number_of_mem_out;
		mem_reg_en_next = mem_reg_en_out;
		operation_next = operation;
	end

	COLUMN_COLLECT_DATA_STATE:begin
		control_next = `COLUMN_COLLECT_DATA_COMMAND;
		start_next = 1'b0;
	end

	FINISH_STATE: begin
		state_finished_next = 1'b1;
		start_next = 1'b0;
	end
	COLUMN_RESET_STATE:begin
		//--Use default settings--//
		start_next = 1'b0;
	end
	endcase
end

endmodule
