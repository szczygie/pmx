`include "pixel_matrix_parameters"
`include "column_ctl_params"

`timescale `TIMESCALE

module column_ctl
	#(
		parameter COLUMN_ADDR = 1
	)
	(
	input clk,
	input reset,
	input [`COLUMN_ADDR_WIDTH-1:0] column_address,		//Address of active column
	input [`CONTROL_WIDTH-1:0] control_in,			//Column CTL control signal
	input [`MEMORY_WIDTH-1:0] data_to_pix_in,		//Data to be send to pixel
	input [`MEMORY_WIDTH-1:0] data_from_pix_in,		//Data from pixel to be send from CTL
	input [`MEMORY_DEPTH_WIDTH-1:0] number_of_mem_cells,	//Number of memory cells to operate on in partial read/write mode
	input mem_reg_en_in,					//Select between 1'b0 - memory and 1'b1 - cfg_reg

	output wire [`PIX_ADDR_WIDTH-1:0] pixel_address,	//Active pixel in column
	output wire [`PIX_CTL_WIDTH-1:0] control_out,		//Pixel control signal
	output wire [`MEMORY_WIDTH-1:0] data_to_pix_out,	//Data sent to pixel
	output wire [`MEMORY_WIDTH-1:0] data_from_pix_out,	//Data from pixel sent from CTL
	output wire pixel_trigger,							//Trigger operation performing in pixel
	output wire mem_reg_en_out,							//Buffered select between 1'b0 - memory and 1'b1 - cfg_reg
	output wire increment_column_address
	);


	wire [`MEMORY_WIDTH-1:0] data_to_pix;
	wire [`MEMORY_WIDTH-1:0] data_from_pix;
	wire [`MEMORY_DEPTH_WIDTH-1:0] memory_cells;
	wire [`PIX_CTL_WIDTH-1:0] control;
	wire mem_reg_en;
	wire done;
	wire [`MEMORY_OPERATION_CLK_CYCELS_WIDTH-1:0] operation_duration;
	
	operation kontroler_wykonywanej_operacji (
		.clk (clk),
		.control (control),
		.control_out (control_out),
		.data_from_pix (data_from_pix),
		.data_from_pix_out(data_from_pix_out),
		.data_to_pix (data_to_pix),
		.data_to_pix_out (data_to_pix_out),
		.done (done),
		.increment_column_address(increment_column_address),
		.memory_cells (memory_cells),
		.mem_reg_en(mem_reg_en),
		.mem_reg_en_out(mem_reg_en_out),
		.operation_duration(operation_duration),
		.pixel_address (pixel_address),
		.pixel_trigger (pixel_trigger),
		.reset(reset)
	);

	operation_ctl #(
		//.COLUMN_ADDR (COLUMN_ADDR),
		.COLUMN_ADDR (COLUMN_ADDR)
		//.COLUMN_COLLECT_DATA_STATE (`COLUMN_COLLECT_DATA_STATE),
		//.COLUMN_WRITE_STATE (`COLUMN_WRITE_STATE),
		//.COLUMN_READ_STATE (`COLUMN_READ_STATE),
		//.COLUMN_READ_AND_RESET_STATE (`COLUMN_READ_AND_RESET_STATE),
		//.COLUMN_READ_AND_RESET_PART_STATE(`COLUMN_READ_AND_RESET_PART_STATE),
		//.COLUMN_READ_PART_STATE (`COLUMN_READ_PART_STATE),
		//.COLUMN_WRITE_PART_STATE (`COLUMN_WRITE_PART_STATE),
		//.COLUMN_RESET_STATE (`COLUMN_RESET_STATE)
	)
	kontroler_trybu_pracy (
		.clk (clk),
		.column_address (column_address),
		.control (control),
		.control_in (control_in),
		.data_from_pix (data_from_pix),
		.data_from_pix_in(data_from_pix_in),
		.data_to_pix (data_to_pix),
		.data_to_pix_in (data_to_pix_in),
		.done (done),
		.memory_cells (memory_cells),
		.mem_reg_en(mem_reg_en),
		.mem_reg_en_in(mem_reg_en_in),
		.number_of_mem_cells(number_of_mem_cells),
		.operation_duration(operation_duration),
		.reset(reset)
	);

endmodule
