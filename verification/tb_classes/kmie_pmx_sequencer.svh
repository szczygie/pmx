/******************************************************************************
* DVT CODE TEMPLATE: sequencer
* Created by szczygie on Oct 7, 2019
* uvc_company = kmie, uvc_name = pmx
*******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_sequencer
`define IFNDEF_GUARD_kmie_pmx_sequencer

//------------------------------------------------------------------------------
//
// CLASS: kmie_pmx_sequencer
//
//------------------------------------------------------------------------------

class kmie_pmx_sequencer extends uvm_sequencer #(kmie_pmx_item_in);
	
	`uvm_component_utils(kmie_pmx_sequencer)

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction : new

endclass : kmie_pmx_sequencer

`endif // IFNDEF_GUARD_kmie_pmx_sequencer
