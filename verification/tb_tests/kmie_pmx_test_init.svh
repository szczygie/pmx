/******************************************************************************
 * Created by szczygie on Oct 16, 2019
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_test_init
`define IFNDEF_GUARD_kmie_pmx_test_init

class kmie_pmx_test_init extends kmie_pmx_base_test;

    `uvm_component_utils(kmie_pmx_test_init)


    function new(string name = "kmie_pmx_test_init", uvm_component parent);
        super.new(name, parent);
    endfunction: new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
    endfunction

    virtual task run_phase(uvm_phase phase);
        kmie_pmx_sequence_init seq_init;
        super.run_phase(phase);
        
        seq_init = kmie_pmx_sequence_init::type_id::create("seq_init");
        
        // needed for setting objections in the base_sequence
        seq_init.set_starting_phase(phase);
        
        seq_init.start(m_env.m_kmie_pmx_agent.m_sequencer);
    endtask

endclass

`endif // IFNDEF_GUARD_kmie_pmx_test_init
