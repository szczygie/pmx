/***********************************************************************************
 * Author:  KSitko
 * File:    pixel.v
 * Description:
 * pixel topmodule
 * works as addr decoder: checks if input pixel address is equal to parameter ADDR
 ***********************************************************************************/

`include "pixel_matrix_parameters" 
`timescale `TIMESCALE

module pixel #(
	parameter ADDR 				= `PIX_0,
	parameter ADDR_WIDTH		= `PIX_ADDR_WIDTH,
	parameter CTL_WIDTH 		= `PIX_CTL_WIDTH,
	
	parameter MEMORY_ADDR_BITS	= `MEMORY_ADDR_BITS,
	parameter MEMORY_WIDTH 		= `MEMORY_WIDTH,
	parameter MEMORY_DEPTH 		= `MEMORY_DEPTH,
	
	parameter CFGREG_REG_NUMBER	= `CFGREG_REG_NUMBER,
	parameter CFGREG_WIDTH 		= `CFGREG_WIDTH,
	
	parameter ADC_DATA_WIDTH	= `ADC_DATA_WIDTH,
	parameter ADC_SAMPLE_COUNT	= `ADC_SAMPLE_COUNT
)
(
	input wire 						clk,				//clk
	input wire [ADDR_WIDTH-1:0] 	addr_in,			//pixel address from matrix_ctl
	input wire [MEMORY_WIDTH-1:0] 	din,				//data to memory or to CfgReg
	input wire 						pixel_trigger,		//matrix_ctl puts '1' here to indicate that pixel should operate
	input wire [CTL_WIDTH-1:0]		op_mode,			//operation mode: 00 IDLE, 01 WRITE, 10 READ_AND_RST, 11 READ
	input wire				 		cfgreg_or_ram,		//0-RAM, 1-CfgReg
	
	output reg [MEMORY_WIDTH-1:0]	dout				//data from RAM or CfgReg to matrix_ctl
);

//--Internal signals--//
reg 							op_perform;				//perform pixel operation, commands pixel to operate
reg								wrong_addr;				//indicates if addr_in doesn't match pixel address
	
wire 							pixel_ctl2ram__read;
wire 							pixel_ctl2ram__write;
wire [MEMORY_ADDR_BITS-1:0]		pixel_ctl2ram__addr;
wire [MEMORY_WIDTH-1:0]			pixel_ctl2ram__ram_din;
wire [MEMORY_WIDTH-1:0]			ram2pixel_ctl__ram_dout;
wire [MEMORY_WIDTH-1:0]			pixel_ctl2matrix_ctl__dout;		
wire [CFGREG_REG_NUMBER-1:0]	pixel_ctl2cfgreg__en;
wire [CFGREG_WIDTH-1:0]			pixel_ctl2cfgreg__din;
wire [(CFGREG_REG_NUMBER*CFGREG_WIDTH)-1:0]	cfgreg2pixel_ctl__cfg;
wire [ADC_DATA_WIDTH-1:0] 		ADC_simple_model2pixel_ctl__data;
wire 							ADC_simple_model2pixel_ctl__data_valid;
wire							pixel_ctl2ADC_simple_model__rst;
wire 							generate_hit_periodically2ADC_simple_model__generate_hit;
wire							comparator2ADC_ctl__comp_result;
wire 							pixel_ctl2ADC_ctl__rst;
wire							ADC_ctl2pixel_ctl__conv_end;
wire [ADC_DATA_WIDTH-1:0] 		ADC_ctl2pixel_ctl__conv_result;
//--Instances--//
pixel_ctl #(
	.CTL_WIDTH(CTL_WIDTH),
	.MEMORY_ADDR_BITS(MEMORY_ADDR_BITS),
	.MEMORY_WIDTH(MEMORY_WIDTH),
	.CFGREG_REG_NUMBER(CFGREG_REG_NUMBER),
	.CFGREG_WIDTH(CFGREG_WIDTH),
	.ADC_DATA_WIDTH(ADC_DATA_WIDTH)
	)
u_pixel_ctl(
	// Inputs
	.clk(clk),
	.din(din),
	.op_mode(op_mode),
	.op_perform(op_perform),
	.wrong_addr(wrong_addr),
	.cfgreg_or_ram(cfgreg_or_ram),
	.ram_dout(ram2pixel_ctl__ram_dout),
	.cfgreg_cfg(cfgreg2pixel_ctl__cfg),
	.ADC_data(ADC_simple_model2pixel_ctl__data),
	.ADC_data_valid(ADC_simple_model2pixel_ctl__data_valid),
	.ADC_ctl_conv_result(ADC_ctl2pixel_ctl__conv_result),
	.ADC_ctl_conv_end(ADC_ctl2pixel_ctl__conv_end),
	// Outputs
	.ram_read(pixel_ctl2ram__read),
	.ram_write(pixel_ctl2ram__write),
	.ram_addr(pixel_ctl2ram__addr),
	.ram_din(pixel_ctl2ram__ram_din),
	.cfgreg_en(pixel_ctl2cfgreg__en),
	.cfgreg_din(pixel_ctl2cfgreg__din),
	.ADC_rst(pixel_ctl2ADC_simple_model__rst),
	.ADC_ctl_rst(pixel_ctl2ADC_ctl__rst),
	.dout2matrix_ctl(pixel_ctl2matrix_ctl__dout)
	);

ram #(
	.ADDR_BITS(MEMORY_ADDR_BITS),
	.DATA_BITS(MEMORY_WIDTH),
	.MEM_SIZE(MEMORY_DEPTH)
	)
u_ram(
	// Inputs
	.read(pixel_ctl2ram__read),
	.write(pixel_ctl2ram__write),
	.addr(pixel_ctl2ram__addr),
	.din(pixel_ctl2ram__ram_din),
	// Outputs
	.dout(ram2pixel_ctl__ram_dout)
	);
	
cfgreg #(
	.CFGREG_REG_NUMBER(CFGREG_REG_NUMBER),
	.CFGREG_WIDTH(CFGREG_WIDTH)
	)
u_cfgreg(
	// Inputs
	.clk(clk),
	.en(pixel_ctl2cfgreg__en),
	.din(pixel_ctl2cfgreg__din),
	// Outputs
	.cfg(cfgreg2pixel_ctl__cfg)
	);
/*
ADC_simple_model #(
	.ADC_DATA_WIDTH(ADC_DATA_WIDTH)
)
u_ADC_simple_model (
	// Inputs
	.clk         (clk),
	.rst         (pixel_ctl2ADC_simple_model__rst),
	.generate_hit(generate_hit_periodically2ADC_simple_model__generate_hit),
	// Outputs
	.adc_data    (ADC_simple_model2pixel_ctl__data),
	.data_valid  (ADC_simple_model2pixel_ctl__data_valid)
);

generate_hit_periodically u_generate_hit_periodically (
	// Inputs
	.op_mode     (op_mode),
	// Outputs
	.generate_hit(generate_hit_periodically2ADC_simple_model__generate_hit)
);
*/
ADC_ctl #(
	.SAMPLE_COUNT(ADC_SAMPLE_COUNT)
)
u_ADC_ctl (
	// Inputs
	.clk          (clk),
	.rst          (pixel_ctl2ADC_ctl__rst),
	.comp_result  (comparator2ADC_ctl__comp_result),
	// Outputs
	.s_b5_x       (),
	.s_bx         (),
	.s_sample_bttm(),
	.s_sample_top (),
	.clk_comp     (),
	.conv_end     (ADC_ctl2pixel_ctl__conv_end),
	.conv_result  (ADC_ctl2pixel_ctl__conv_result)
);
//--Decode pixel address--//
always @(posedge clk) begin :addr_decode
	op_perform	<= `FALSE;
	wrong_addr	<= `FALSE;
	if (addr_in == ADDR) begin
		if (pixel_trigger == `TRUE) begin
			op_perform	<= `TRUE;
		end
		else begin
			//empty
		end
	end
	else begin
		wrong_addr	<= `TRUE;
	end
end
//--Connect instances signals--//
always@* begin
	dout	= pixel_ctl2matrix_ctl__dout;
end

endmodule
