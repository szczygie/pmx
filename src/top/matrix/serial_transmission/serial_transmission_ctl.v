`include "column_ctl_params"
`include "pixel_matrix_parameters"
`include "serial_transmission_parameters"

module serial_transmission_ctl(
	input clk,
	input rst,
	input sin,
	input op_done,
	input [`MEMORY_WIDTH-1:0] data_from_matrix,
    output [(`GLOBAL_CFGREG_REG_NUMBER*`GLOBAL_CFGREG_WIDTH)-1:0] cfg,
	output reg sout,
	output [`MEMORY_WIDTH-1:0] data,
	output [`MEMORY_DEPTH_WIDTH+`CONTROL_WIDTH:0] ctl
);

wire [`GLOBAL_CFGREG_WIDTH-1:0] dac_reg_in;
wire [`GLOBAL_CFGREG_WIDTH+`GLOBAL_CFGREG_EN_WIDTH-1:0] dac_reg_out;
wire dac_read;
wire dac_write;
wire [`CONTROL_WIDTH-1:0] ctl_delayed;

wire sout_from_sr;
wire [`MEMORY_WIDTH+`HEADER_WIDTH-1:0] sout_from_matrix;
wire out_in_sel;
wire sout_out;

global_cfgreg #(.CFGREG_REG_NUMBER(`GLOBAL_CFGREG_REG_NUMBER),
	 .CFGREG_WIDTH(`GLOBAL_CFGREG_WIDTH)) 
Xcfgreg(
	 .rst(rst),
	 .clk(clk),
	 .en(dac_reg_out[`GLOBAL_CFGREG_WIDTH+:4]),
	 .write(dac_write),
	 .din(dac_reg_out[`GLOBAL_CFGREG_WIDTH-1:0]),
	 .cfg(cfg)
);

multiplexer #( 	.INPUTS_NR(16),
		.INPUT_WIDTH(`GLOBAL_CFGREG_WIDTH),
		.SELECT_WIDTH(4))
Xcfg_multiplex(
		.din(cfg),
		.select(dac_reg_out[`GLOBAL_CFGREG_WIDTH+:4]),
		.dout(dac_reg_in)
);

in_fsm Xin_fsm(
	.clk(clk),
	.rst(rst),
	.sin(sin),
	.dac_reg_in(dac_reg_in),
	.sout(sout_from_sr),
	.data(data),
	.ctl(ctl),
	.dac_reg_out(dac_reg_out),
	.dac_read(dac_read),
	.dac_write(dac_write)
);

delay #(
	.DELAY_NR    (32),
	.SIGNAL_WIDTH(`CONTROL_WIDTH)
)
ctl_delay (
	.clk            (clk),
	.delayed_signal (ctl_delayed),
	.signal_to_delay(ctl[`CONTROL_WIDTH-1:0])
	);


out_fsm Xout_fsm(
	.clk(clk),
	.reset(rst),
	.data_in(data_from_matrix),
	.ctl(ctl_delayed),
	.op_done(op_done),
	.data_out(sout_from_matrix),
	.out_in_sel(out_in_sel)
);

multiplexer #( 	.INPUTS_NR(2),
		.INPUT_WIDTH(1),
		.SELECT_WIDTH(1))
Xsout_multiplex(
		.din({sout_from_matrix[`MEMORY_WIDTH+`HEADER_WIDTH-1],sout_from_sr}),
		.select(out_in_sel),
		.dout(sout_out)
);

//sout flop
always @(posedge clk) begin
	sout <= sout_out;
	if(rst)begin
		sout <= 1'b1;
	end
end

endmodule
