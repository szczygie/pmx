`timescale 1ns/1ns
`include "column_ctl_params"
`include "pixel_matrix_parameters"

module serial_transmission(
	input clkx1, 
	input clkx4, 
	input reset, 
	input sin, 
	output sout, 
	input swrite, 
	input operation_finished, 
	input [`MEMORY_WIDTH-1:0] data_from_pix, 
	output [`MEMORY_WIDTH-1:0] data_to_pix, 
	output [`CONTROL_WIDTH-1:0] control, 
	output mem_reg_en, 
	output [`MEMORY_DEPTH_WIDTH-1:0] number_of_mem
);

wire [`SHFIT_REG_WIDTH-1:0] pdata_2ctl;
wire [`SHFIT_REG_WIDTH-1:0] pdata_2sr;
wire pwrite;

shift_register_ctl Xshift_register_ctl(
	.clk(clkx1),
	.reset(reset),
	.sr_pin(pdata_2ctl),
	.sr_pout(pdata_2sr),
	.sr_pwrite(pwrite),
	.pix_operation_finished(operation_finished),
	.pix_data_from_pix(data_from_pix),
	.pix_data_to_pix(data_to_pix),
	.pix_control(control),
	.pix_mem_reg_en(mem_reg_en),
	.pix_number_of_mem(number_of_mem)
);


shift_register #(.SHFIT_REG_WIDTH(16)) Xshift_register(
	.clk(clkx4),
	.rst(reset),
	.sin(sin),
	.swrite(swrite),
	.pin(pdata_2sr),
	.pwrite(pwrite),
	.sout(sout),
	.pout(pdata_2ctl)
);

endmodule
