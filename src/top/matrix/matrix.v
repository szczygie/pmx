// History:
// 2019-12-14 RSz. Reset buffer for each column separately

`timescale 1ns/1ns

`include "pixel_matrix_parameters"
`include "column_ctl_params"

module matrix(
    input  wire                                                        clkx1,
    input  wire [`COLUMNS_IN_MATRIX-1:0]                               clkx1col, // divided clock for columns
    input  wire                                                        clkx4,
    input  wire                                                        sin,
    input  wire                                                        rst,
    input  wire                                                        b_strobe,
    output      [(`GLOBAL_CFGREG_REG_NUMBER*`GLOBAL_CFGREG_WIDTH)-1:0] cfg,
    output wire                                                        sout
);

wire   [`MEMORY_WIDTH-1:0]                      in_data_to_pix;
wire   [`MEMORY_DEPTH_WIDTH-1:0]                in_number_of_mem;
wire                                            in_mem_reg_en;
wire   [`CONTROL_WIDTH-1:0]                     in_control;
wire   [`MEMORY_WIDTH-1:0]                      out_data_from_pix;
wire   [`COLUMNS_IN_MATRIX*`MEMORY_WIDTH-1:0]   inpix_data_from_pix;
wire   [`COLUMNS_IN_MATRIX*`MEMORY_WIDTH-1:0]   outpix_data_to_pix;
wire   [`COLUMNS_IN_MATRIX*`PIX_ADDR_WIDTH-1:0] outpix_pixel_address;
wire   [`COLUMNS_IN_MATRIX-1:0]                 outpix_pixel_trigger;
wire   [`COLUMNS_IN_MATRIX-1:0]                 outpix_mem_reg_en;
wire   [`COLUMNS_IN_MATRIX*`PIX_CTL_WIDTH-1:0]  outpix_control;

wire                                            operation_finished;

reg    [`COLUMNS_IN_MATRIX-1:0]                 rst_col;
reg                                             rst_kontroler;

serial_transmission_ctl Xserial_transmission_ctl(
    .clk(clkx4),
    .rst(rst),
    .sin(sin),
    .op_done(operation_finished),
    .data_from_matrix(out_data_from_pix),
    .sout(sout),
    .data(in_data_to_pix),
    .ctl({in_number_of_mem,in_mem_reg_en,in_control}),
    .cfg(cfg)
);


matrix_ctl kontroler_matrycy_pikseli (
    .clk (clkx1),
    .reset (rst_kontroler),
    .in_control (in_control),
    .in_data_to_pix (in_data_to_pix),
    .in_mem_reg_en (in_mem_reg_en),
    .in_number_of_mem (in_number_of_mem),
    .inpix_data_from_pix (inpix_data_from_pix),
    .out_data_from_pix (out_data_from_pix),
    .outpix_control (outpix_control),
    .outpix_data_to_pix (outpix_data_to_pix),
    .outpix_mem_reg_en (outpix_mem_reg_en),
    .outpix_pixel_address(outpix_pixel_address),
    .outpix_pixel_trigger(outpix_pixel_trigger),
    .out_finished(operation_finished)
);


genvar                                          j;
generate

    for(j=0; j<`COLUMNS_IN_MATRIX; j=j+1) begin: column

        column u_column (                            // Fixed params for hierarchical flow
            .addr_in (outpix_pixel_address[(j+1)*`PIX_ADDR_WIDTH-1 -: `PIX_ADDR_WIDTH]), //pixel address from matrix_ctl
            .cfgreg_or_ram(outpix_mem_reg_en[j]),    //0-RAM, 1-CfgReg
            .clk (clkx1col[j]),                      //clk
            .rst (rst_col[j]),
            .din (outpix_data_to_pix[(j+1)*`MEMORY_WIDTH-1 -: `MEMORY_WIDTH]), //data to memory or to CfgReg
            .dout (inpix_data_from_pix[(j+1)*`MEMORY_WIDTH-1 -: `MEMORY_WIDTH]), //data from RAM or CfgReg to matrix_ctl
            .op_mode (outpix_control[(j+1)*`PIX_CTL_WIDTH-1 -: `PIX_CTL_WIDTH]), //operation mode: 00 IDLE, 01 WRITE, 10 READ_AND_RST, 11 READ
            .pixel_trigger(outpix_pixel_trigger[j]), //matrix_ctl puts '1' here to indicate that pixel should operate
            .b_strobe(b_strobe)
        );
    end

endgenerate

// Buffer the reset
always @(posedge clkx1) begin:rst_buffer
    rst_kontroler <= rst;
    rst_col       <= {`COLUMNS_IN_MATRIX{rst}};
end

endmodule
