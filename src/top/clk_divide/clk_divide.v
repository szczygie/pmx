//------------------------------------------------------------------------------
// Clock divider and reset synchronizer module
// Author: Robert Szczygiel 2019 @ AGH UST
//------------------------------------------------------------------------------
`include "column_ctl_params"

module clk_divide
#( parameter
    CLK_DIVIDE_BY   = 4,
    RST_IN_MIN_LEN  = 4, // minimum number of clk cycles rst should be active to trigger the reset
    RST_OUT_MIN_LEN = 4  // minimum number of clk_div cycles rst_synced should be active
)
(
    input  wire                          clk,
    input  wire                          rst,       // sync reset active high
    output reg                           clk_div,   // divided clock
    output reg  [`COLUMNS_IN_MATRIX-1:0] clk_div_col, // divided clock for columns
    output reg                           rst_synced // reset will be activated after RST_IN_MIN_LEN number of clk cycles
// reset will removed 1 clk period after rising edge
// of the clk_div
);

//------------------------------------------------------------------------------
// main parameters

//------------------------------------------------------------------------------
// local paramters
localparam                      RST_COUNTER_BITS = $clog2(RST_OUT_MIN_LEN);
localparam                      CLK_COUNTER_BITS = $clog2(CLK_DIVIDE_BY);
localparam                      CLK_HALF_RANGE   = CLK_DIVIDE_BY / 2;

//------------------------------------------------------------------------------
// local variables
reg        [RST_IN_MIN_LEN-1:0] rst_sh_in;
reg        [RST_COUNTER_BITS:0] rst_counter;
reg        [CLK_COUNTER_BITS:0] clk_counter;
reg                             clk_div_prev;                              // previous value of the output clock

reg                             clk_div_i;

wire                            clk_div_edge;
assign clk_div_edge = clk_div_i & (!clk_div_prev);

//------------------------------------------------------------------------------
// input rst shift registers
always @(posedge clk) begin
    rst_sh_in <= {rst_sh_in[RST_IN_MIN_LEN-2:0], rst};
end

//------------------------------------------------------------------------------
// reset counter
always @(posedge clk) begin
    if(& rst_sh_in)
        rst_counter <= RST_OUT_MIN_LEN;
    else if( rst_counter == 0)
        rst_counter <= 0;
    else
        rst_counter <= clk_div_edge ? rst_counter - 1 : rst_counter;
end

//------------------------------------------------------------------------------
// output reset
always @(posedge clk) begin
    if( &rst_sh_in ) // activate reset
        rst_synced <= 1;
    else if( !(|rst_counter) )
        rst_synced <= 0;
    else
        rst_synced <= rst_synced;
end

//------------------------------------------------------------------------------
// clock counter
always @(posedge clk) begin
    if( (& rst_sh_in) || (clk_counter == 0) )begin
        clk_counter <= CLK_DIVIDE_BY - 1;
        clk_div_i   <= 0;
        clk_div     <= 0;
        clk_div_col <= {`PIX_IN_COLUMN{1'b0}};
    end
    else if(clk_counter == CLK_HALF_RANGE) begin
        clk_counter <= clk_counter - 1;
        clk_div_i   <= 1;
        clk_div     <= 1;
        clk_div_col <= {`PIX_IN_COLUMN{1'b1}};
    end
    else begin
        clk_counter <= clk_counter - 1;
        clk_div     <= clk_div_i;
        clk_div_col <= {`PIX_IN_COLUMN{clk_div_i}};
    end
end
//------------------------------------------------------------------------------
// buffer output clock to detect the edge
always @(posedge clk)
    clk_div_prev <= clk_div_i;

//------------------------------------------------------------------------------
endmodule
