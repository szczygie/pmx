/******************************************************************************
 * DVT CODE TEMPLATE: monitor
 * Created by szczygie on Oct 7, 2019
 * uvc_company = kmie, uvc_name = pmx
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_monitor_in
`define IFNDEF_GUARD_kmie_pmx_monitor_in

//------------------------------------------------------------------------------
//
// CLASS: kmie_pmx_monitor_in
//
//------------------------------------------------------------------------------

class kmie_pmx_monitor_in extends kmie_pmx_monitor_base#(.T(kmie_pmx_item_in));

    `uvm_component_utils(kmie_pmx_monitor_in)

    function new (string name, uvm_component parent);
        super.new(name, parent);
    endfunction : new


    virtual task collect_items();
        forever begin
            m_kmie_pmx_vif.get_dut_input_data(m_collected_item);
            `uvm_info(get_full_name(), $sformatf("\n%s", m_collected_item.sprint()), UVM_HIGH)
            m_collected_item_port.write(m_collected_item);
            if (m_config_obj.m_checks_enable)
                perform_item_checks();
        end
    endtask : collect_items


    virtual function void perform_item_checks();
    // Perform item checks here, if any
    endfunction : perform_item_checks


endclass : kmie_pmx_monitor_in

`endif // IFNDEF_GUARD_kmie_pmx_monitor_in
