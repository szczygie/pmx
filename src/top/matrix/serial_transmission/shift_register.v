//***************************************//
// 	       Shift Register
//**************************************//
`timescale 1ns/1ns
`include "column_ctl_params"
`include "pixel_matrix_parameters"

module shift_register 
#(parameter SHFIT_REG_WIDTH = 16)(
	input clk,
	input rst,

	// Serial
	input sin,
	input swrite,
	output reg sout,

	//Parallel
	input [SHFIT_REG_WIDTH-1:0] pin,
	input pwrite,
	output reg [SHFIT_REG_WIDTH-1:0] pout
);

reg [SHFIT_REG_WIDTH-1:0] pout_internal;
reg [SHFIT_REG_WIDTH-1:0] pin_internal;

reg [SHFIT_REG_WIDTH-1:0] pout_next;
reg sout_next;

//SerialIN - ParallelOUT
//Output logic
always @(posedge clk) begin
	pout <= pout_next;
	// Reset logic
	if(rst) begin
		pout <= {SHFIT_REG_WIDTH{1'b0}};
	end	
end

//Internal signal logic
always @(posedge clk) begin
	pout_internal <= {sin,pout_internal[SHFIT_REG_WIDTH-1:1]};
	if(rst) begin
		pout_internal <= {SHFIT_REG_WIDTH{1'b0}};
	end
end

//Next output logic
always @* begin
	pout_next <= pout;
	if(swrite) begin
		pout_next <= pout_internal;
	end
end

//ParallelIN - SerialOUT
//Output logic
always @(posedge clk) begin
	sout <= sout_next;
	if(rst) begin
		sout <= 1'b0;
	end
end

//Internal signal logic
always @(posedge clk) begin
	pin_internal <= pin_internal >> 1;
	if(pwrite) begin
		pin_internal <= pin;
	end
	if(rst) begin
		pin_internal <= {SHFIT_REG_WIDTH{1'b0}};
	end
end

always @* begin
	sout_next <= pin_internal[0];
end


endmodule
