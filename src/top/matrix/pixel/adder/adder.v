`include "pixel_matrix_parameters"
`timescale `TIMESCALE

module adder #(
	parameter DATA_WIDTH = 12
	)
	(
	input wire [DATA_WIDTH-1:0] din,
	output wire [DATA_WIDTH-1:0] dout 
);
	
	assign dout = din + 1'b1;

endmodule
