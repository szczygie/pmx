/******************************************************************************
 * DVT CODE TEMPLATE: example test
 * Created by szczygie on Oct 8, 2019
 * uvc_company = kmie, uvc_name = pmx
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_example_test
`define IFNDEF_GUARD_kmie_pmx_example_test

class kmie_pmx_example_test extends kmie_pmx_base_test;

    `uvm_component_utils(kmie_pmx_example_test)

    function new(string name = "kmie_pmx_example_test", uvm_component parent);
        super.new(name, parent);
    endfunction: new

    virtual function void build_phase(uvm_phase phase);
        uvm_config_db#(uvm_object_wrapper)::set(this,
            "m_env.m_kmie_pmx_agent.m_sequencer.run_phase",
            "default_sequence",
            default_sequence_class::type_id::get());

        // Create the env
        super.build_phase(phase);
    endfunction

endclass

class default_sequence_class extends kmie_pmx_base_sequence;

    `uvm_object_utils(default_sequence_class)

    function new(string name = "default_sequence_class");
        super.new(name);
    endfunction : new

    virtual task body();
        `uvm_info(get_type_name(), "Executing default sequence", UVM_MEDIUM)
        repeat (100) begin
            `uvm_do(req)
        end
    endtask : body

endclass : default_sequence_class

`endif // IFNDEF_GUARD_kmie_pmx_example_test
