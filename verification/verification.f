#

# PMX chip design files
# -F ../src/src.f
# or dummy top:
top_dummy.sv

-uvm
-uvmhome /cad/XCELIUM1809/tools/methodology/UVM/CDNS-1.2
# testbench files
-incdir tb_classes
-incdir tb_tests
-incdir tb_ref_model

-uvmlinedebug
-timescale 1ns/1ps

kmie_pmx_pkg.sv
kmie_pmx_if.sv
kmie_pmx_tb_top.sv
