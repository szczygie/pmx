/******************************************************************************
 * DVT CODE TEMPLATE: agent
 * Created by szczygie on Oct 7, 2019
 * uvc_company = kmie, uvc_name = pmx
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_agent
`define IFNDEF_GUARD_kmie_pmx_agent

//------------------------------------------------------------------------------
//
// CLASS: kmie_pmx_agent
//
//------------------------------------------------------------------------------

class kmie_pmx_agent extends uvm_agent;

    // Configuration object
    protected kmie_pmx_config_obj m_config_obj;

    kmie_pmx_driver m_driver;
    kmie_pmx_sequencer m_sequencer;
    kmie_pmx_monitor_in m_monitor_in;
    kmie_pmx_monitor_out m_monitor_out;
    kmie_pmx_coverage_collector m_coverage_collector;

    `uvm_component_utils(kmie_pmx_agent)

    function new (string name, uvm_component parent);
        super.new(name, parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);

        // Get the configuration object
        if(!uvm_config_db#(kmie_pmx_config_obj)::get(this, "", "m_config_obj", m_config_obj))
            `uvm_fatal("NOCONFIG", {"Config object must be set for: ", get_full_name(), ".m_config_obj"})

        // Propagate the configuration object to monitor
        uvm_config_db#(kmie_pmx_config_obj)::set(this, "m_monitor_in", "m_config_obj", m_config_obj);
        // Create the monitor
        m_monitor_in = kmie_pmx_monitor_in::type_id::create("m_monitor_in", this);
        uvm_config_db#(kmie_pmx_config_obj)::set(this, "m_monitor_out", "m_config_obj", m_config_obj);
        // Create the monitor
        m_monitor_out = kmie_pmx_monitor_out::type_id::create("m_monitor_out", this);

        if(m_config_obj.m_coverage_enable) begin
            m_coverage_collector = kmie_pmx_coverage_collector::type_id::create("m_coverage_collector", this);
        end
        
        if(m_config_obj.m_is_active == UVM_ACTIVE) begin
            // Propagate the configuration object to driver
            uvm_config_db#(kmie_pmx_config_obj)::set(this, "m_driver", "m_config_obj", m_config_obj);
            // Create the driver
            m_driver             = kmie_pmx_driver::type_id::create("m_driver", this);

            // Create the sequencer
            m_sequencer          = kmie_pmx_sequencer::type_id::create("m_sequencer", this);
        end
    endfunction : build_phase

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        if(m_config_obj.m_coverage_enable) begin
            m_monitor_in.m_collected_item_port.connect(m_coverage_collector.m_monitor_port);
        end

        if(m_config_obj.m_coverage_enable == UVM_ACTIVE) begin
            m_driver.seq_item_port.connect(m_sequencer.seq_item_export);
        end
        
    endfunction : connect_phase

endclass : kmie_pmx_agent

`endif // IFNDEF_GUARD_kmie_pmx_agent
