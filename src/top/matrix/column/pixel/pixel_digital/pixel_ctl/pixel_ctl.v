`include "pixel_matrix_parameters"
`timescale `TIMESCALE

module pixel_ctl #(
	parameter CTL_WIDTH			= `PIX_CTL_WIDTH,	
	parameter MEMORY_ADDR_BITS	= `MEMORY_ADDR_BITS,
	parameter MEMORY_WIDTH	 	= `MEMORY_WIDTH,
	parameter CFGREG_REG_NUMBER	= `CFGREG_REG_NUMBER,
	parameter CFGREG_WIDTH 		= `CFGREG_WIDTH,
	parameter ADC_DATA_WIDTH	= `ADC_DATA_WIDTH
	)
	(				
	input wire 														clk, 				//clock signal
	input wire														rst,				//rst
	input wire [MEMORY_WIDTH-1:0] 				din,				//data to RAM or to CfgReg
	input wire [CTL_WIDTH-1:0] 						op_mode,			//operation mode: 00 IDLE, 01 WRITE, 10 READ_AND_RST, 11 READ
	input wire 														op_perform,			//perform operation coded in op_mode 
	input wire 														wrong_addr,			//1 if wrong pixel address was set, used to go to IDLE after changing pixel which is operating 
	input wire 														cfgreg_or_ram,		//0-RAM, 1-CfgReg
	 
	input wire [ADC_DATA_WIDTH-1:0]			ADC_conv_result,	//ADC data is an address to the RAM memory
	input wire 														DISCRout,					//indicates that data from ADC is ready
	input wire 														EOC, 							//End of Conversion. ADC produced proper output
	output reg 													ADC_start_conv,		//starts ADC conversion
	
	input wire [MEMORY_WIDTH-1:0] 				ram_dout,			//data read from RAM to pixel_ctl
	output reg 													ram_read,			//active high
	output reg 													ram_write,			//active high
	output reg [MEMORY_ADDR_BITS-1:0]	ram_addr,			//ram address
	output reg [MEMORY_WIDTH-1:0] 			ram_din,			//data from pixel_ctl to be written in RAM
	
	input wire [(CFGREG_REG_NUMBER*CFGREG_WIDTH)-1:0] 	cfgreg_cfg, 		//all data stored in CfgReg. It is multiplexed when reading cfgreg
	output reg [CFGREG_REG_NUMBER-1:0]									cfgreg_en,			//one hot encoding, enables writing to CfgReg
	output reg [CFGREG_WIDTH-1:0] 												cfgreg_din,			//data from pixel_ctl to be written in CfgReg 	
	
	output reg [MEMORY_WIDTH-1:0] 												dout2matrix_ctl		//data to matrix_ctl
	);
//--Localparams--//
localparam IDLE				= `PIX_IDLE;
localparam WRITE			= `PIX_WRITE;
localparam READ				= `PIX_READ;
localparam READ_AND_RESET	= `PIX_READ_AND_RESET;
localparam COLLECT_DATA		= `PIX_COLLECT_DATA;
localparam CYCLE0 			= 3'b000;
localparam CYCLE1 			= 3'b001;
localparam CYCLE2 			= 3'b010;
localparam CYCLE3 			= 3'b011;
localparam CYCLE4				= 3'b100;
localparam CYCLE5				= 3'b101;
localparam CYCLE6				= 3'b110;
localparam CYCLE7				= 3'b111;
//--Registered outputs (RAM)--//
reg 						nxt_ram_read;
reg 						nxt_ram_write;
reg [MEMORY_ADDR_BITS-1:0] 	nxt_ram_addr;
reg [MEMORY_WIDTH-1:0] 		nxt_ram_din;
//--Registered outputs (RCfgReg)--//
reg [CFGREG_REG_NUMBER-1:0]	nxt_cfgreg_en;
reg [CFGREG_WIDTH-1:0] 		nxt_cfgreg_din; 
//--Registered outputs (ADC_simple_model)--//
reg 						nxt_ADC_start_conv;
//--Registered outputs--//
reg [MEMORY_WIDTH-1:0]		nxt_dout2matrix_ctl;
//--Internal signals--// 
reg	[CTL_WIDTH-1:0] 		state, nxt_state;
reg	[2:0]				 	cycle_ctr, nxt_cycle_ctr;
reg [MEMORY_WIDTH-1:0]		adder_din, nxt_adder_din; //it is used in adder to handle memory address incrementing and also in COLLECT_DATA to increment counters, so it has to be MEMORY_WIDTH long, not only MEMORY_ADDR_BITS long
wire [MEMORY_WIDTH-1:0]		adder_incremented;
reg [CFGREG_REG_NUMBER-1:0] cfgreg_en_int, nxt_cfgreg_en_int;
//--Instances--//
adder #(
	.DATA_WIDTH(MEMORY_WIDTH)
	)
u_adder(
	.din(adder_din),
	.dout(adder_incremented)
	);
//--FSM--//
//--State register--//
always @(posedge clk) begin
	if (rst) begin
		state		<= IDLE;
	end
	else begin
		state		<= nxt_state;
	end
end
//--Next state logic--//
always @* begin
	nxt_state	= IDLE;
	if (op_mode == COLLECT_DATA) begin
		nxt_state = COLLECT_DATA;
	end
	else begin
		if (op_perform==`TRUE) begin 
			nxt_state	= op_mode;
		end
		else if (wrong_addr == `FALSE) begin		
			nxt_state	= (op_mode == IDLE) ? IDLE : state;
		end
		else begin
			nxt_state	= IDLE;
		end
	end
end
//--Output register + internal signals--//
always @(posedge clk) begin
	if (rst) begin
		ram_read 				<= `FALSE;
		ram_write 				<= `FALSE;
		ram_addr 				<= {MEMORY_ADDR_BITS{1'b0}};
		ram_din 					<= {MEMORY_WIDTH{1'b0}};
		
		cfgreg_en_int		<= {CFGREG_REG_NUMBER{1'b0}};
		cfgreg_en 				<= {CFGREG_REG_NUMBER{1'b0}};
		cfgreg_din 			<= {CFGREG_WIDTH{1'b0}};
		
		ADC_start_conv	<= `FALSE;
		
		adder_din				<= {MEMORY_WIDTH{1'b0}};
		dout2matrix_ctl	<= {MEMORY_WIDTH{1'b0}};
		cycle_ctr					<= CYCLE0;
	end
	else begin
		ram_read 				<= nxt_ram_read;
		ram_write 				<= nxt_ram_write;
		ram_addr 				<= nxt_ram_addr;
		ram_din 					<= nxt_ram_din;
		
		cfgreg_en_int		<= nxt_cfgreg_en_int;
		cfgreg_en 				<= nxt_cfgreg_en;
		cfgreg_din 			<= nxt_cfgreg_din;
		
		ADC_start_conv	<= nxt_ADC_start_conv;
		
		adder_din				<= nxt_adder_din;
		dout2matrix_ctl	<= nxt_dout2matrix_ctl;
		cycle_ctr					<= nxt_cycle_ctr;
	end
end
//--Output logic--//
always @* begin 	
	nxt_ram_read		= `FALSE;
	nxt_ram_write		= `FALSE;
	nxt_ram_addr		= ram_addr;	//nie daje tu zer, bo by sie musialo czesciej przelaczac wszystko, np. w WRITE CYCLE2 bym to wywalal na 0 i to by powodowalo wieksze zuzycie energii, takie przelaczanie. tak mi sie wydaje. prze din tak samo i przy cfgreg tez
	nxt_ram_din			= ram_din;
	
	nxt_cfgreg_en_int	= cfgreg_en_int;
	nxt_cfgreg_en		= {CFGREG_REG_NUMBER{1'b0}};
	nxt_cfgreg_din		= cfgreg_din;
	
	nxt_ADC_start_conv	= `FALSE;
	
	nxt_adder_din		= {{MEMORY_WIDTH-MEMORY_ADDR_BITS{1'b0}},ram_addr}; 
	nxt_dout2matrix_ctl = dout2matrix_ctl;
	nxt_cycle_ctr		= CYCLE0;
	
	case(nxt_state)
		IDLE: begin
				nxt_adder_din		= {MEMORY_WIDTH{1'b0}};
				nxt_cfgreg_en_int	= {CFGREG_REG_NUMBER{1'b0}};
				
				nxt_ram_addr		= {MEMORY_ADDR_BITS{1'b0}};
				nxt_ram_din			= {MEMORY_WIDTH{1'b0}};
				nxt_cfgreg_din		= {CFGREG_WIDTH{1'b0}};
				nxt_dout2matrix_ctl	= {MEMORY_WIDTH{1'b0}};
			end
		COLLECT_DATA: begin
				case(cycle_ctr)
					CYCLE0: begin
						nxt_cycle_ctr					= (DISCRout == `TRUE) ? CYCLE1 : CYCLE0;
					end
					CYCLE1: begin
						nxt_ADC_start_conv		= `TRUE;
						nxt_cycle_ctr					= CYCLE2;
					end
					CYCLE2: begin
						nxt_ram_addr				= ADC_conv_result;
						nxt_cycle_ctr					= (EOC == `TRUE) ? CYCLE3 : CYCLE2;
					end
					CYCLE3: begin
						nxt_ram_read				= `TRUE;
						nxt_cycle_ctr					= CYCLE4;
					end
					CYCLE4: begin
						nxt_adder_din				= ram_dout;
						nxt_cycle_ctr					= CYCLE5;
					end
					CYCLE5: begin
						nxt_adder_din				= ram_dout; //keep proper input to adder, because adder is on assign and has NO registered output
						nxt_ram_din					= adder_incremented;
						nxt_cycle_ctr					= CYCLE6;
					end
					CYCLE6: begin
						nxt_ram_write				= `TRUE;
						nxt_cycle_ctr					= (DISCRout == `FALSE) ? CYCLE0 : CYCLE7;
					end
					CYCLE7: begin
						nxt_cycle_ctr					= (DISCRout == `FALSE) ? CYCLE0 : CYCLE7;
					end
				endcase
			end
		WRITE: begin //3 cycles
					case(cycle_ctr)
						CYCLE0: begin
								if (cfgreg_or_ram) begin
									if(op_perform == `TRUE)begin
										nxt_cfgreg_en_int	= (cfgreg_en_int == {CFGREG_REG_NUMBER{1'b0}}) ? {{(CFGREG_REG_NUMBER-1){1'b0}}, 1'b1} : (cfgreg_en_int == {1'b1, {(CFGREG_REG_NUMBER-1){1'b0}}}) ? {{(CFGREG_REG_NUMBER-1){1'b0}}, 1'b1}  : (cfgreg_en_int << 1); //(cfgreg_en_int ==  {1'b1, {(CFGREG_REG_NUMBER-1){1'b0}}}) ?  {{(CFGREG_REG_NUMBER-1){1'b0}}, 1'b1} : (cfgreg_en_int << 1);	//because if doesn't go from 1000 to 0001
									end
									else begin
										nxt_cfgreg_en_int = cfgreg_en_int;
									end
									nxt_cfgreg_din		= din;
								end
								else begin
									nxt_ram_din			= din;
								end
								nxt_cycle_ctr			= (op_perform == `TRUE) ? CYCLE1 : CYCLE0;
							end
						CYCLE1: begin
								if (cfgreg_or_ram) begin
									nxt_cfgreg_en		= cfgreg_en_int;
								end
								else begin
									nxt_ram_write		= `TRUE;
								end
								nxt_cycle_ctr			= CYCLE2;
							end
						CYCLE2: begin
								if (cfgreg_or_ram) begin
									//wait
								end
								else begin
									nxt_ram_addr		= adder_incremented[MEMORY_ADDR_BITS-1:0];
								end
								//next trigger from matrix_ctl must be now
							end
					endcase
				end	
		READ: begin //3 cycles
					case(cycle_ctr)
						CYCLE0: begin
								if (cfgreg_or_ram) begin
									if(op_perform == `TRUE)begin
										nxt_cfgreg_en_int	= (cfgreg_en_int == {CFGREG_REG_NUMBER{1'b0}}) ? {{(CFGREG_REG_NUMBER-1){1'b0}}, 1'b1} : (cfgreg_en_int == {1'b1, {(CFGREG_REG_NUMBER-1){1'b0}}}) ? {{(CFGREG_REG_NUMBER-1){1'b0}}, 1'b1}  : (cfgreg_en_int << 1); //(cfgreg_en_int ==  {1'b1, {(CFGREG_REG_NUMBER-1){1'b0}}}) ?  {{(CFGREG_REG_NUMBER-1){1'b0}}, 1'b1} : (cfgreg_en_int << 1);	//because if doesn't go from 1000 to 0001
									end
									else begin
										nxt_cfgreg_en_int = cfgreg_en_int;
									end
								end
								else begin
									nxt_ram_read		= `TRUE;
								end
								nxt_cycle_ctr			= (op_perform == `TRUE) ? CYCLE1 : CYCLE0;
							end
						CYCLE1: begin
								if (cfgreg_or_ram) begin	//CfgReg multiplexer
									nxt_dout2matrix_ctl	= (cfgreg_en_int == 4'b0001) ? cfgreg_cfg[(1*CFGREG_WIDTH)-1:(0*CFGREG_WIDTH)] :
														  (cfgreg_en_int == 4'b0010) ? cfgreg_cfg[(2*CFGREG_WIDTH)-1:(1*CFGREG_WIDTH)] :
														  (cfgreg_en_int == 4'b0100) ? cfgreg_cfg[(3*CFGREG_WIDTH)-1:(2*CFGREG_WIDTH)] :
														  							   cfgreg_cfg[(4*CFGREG_WIDTH)-1:(3*CFGREG_WIDTH)]; //4'b1000
								end
								else begin
									nxt_dout2matrix_ctl = ram_dout;
								end
								nxt_cycle_ctr			= CYCLE2;
							end
						CYCLE2: begin
								if (cfgreg_or_ram) begin
									//nothing, CfgReg already serviced
								end
								else begin
									nxt_ram_addr		= adder_incremented[MEMORY_ADDR_BITS-1:0];
								end
								//next trigger from matrix_ctl must be now
							end
					endcase
				end
		READ_AND_RESET: begin //4 cycles 
					case(cycle_ctr)
						CYCLE0: begin
								if (cfgreg_or_ram) begin
									if(op_perform == `TRUE)begin
										nxt_cfgreg_en_int	= (cfgreg_en_int == {CFGREG_REG_NUMBER{1'b0}}) ? {{(CFGREG_REG_NUMBER-1){1'b0}}, 1'b1} : (cfgreg_en_int == {1'b1, {(CFGREG_REG_NUMBER-1){1'b0}}}) ? {{(CFGREG_REG_NUMBER-1){1'b0}}, 1'b1}  : (cfgreg_en_int << 1); //(cfgreg_en_int ==  {1'b1, {(CFGREG_REG_NUMBER-1){1'b0}}}) ?  {{(CFGREG_REG_NUMBER-1){1'b0}}, 1'b1} : (cfgreg_en_int << 1);	//because if doesn't go from 1000 to 0001
										nxt_cfgreg_din		= {CFGREG_WIDTH{1'b0}};
									end
									else begin
										nxt_cfgreg_en_int = cfgreg_en_int;
									end
								end
								else begin
									nxt_ram_din			= {MEMORY_WIDTH{1'b0}};
									nxt_ram_read		= `TRUE;
								end
								nxt_cycle_ctr			= (op_perform == `TRUE) ? CYCLE1 : CYCLE0;
							end
						CYCLE1: begin
								if (cfgreg_or_ram) begin
									nxt_dout2matrix_ctl	= (cfgreg_en_int == 4'b0001) ? cfgreg_cfg[(1*CFGREG_WIDTH)-1:(0*CFGREG_WIDTH)] :
														  (cfgreg_en_int == 4'b0010) ? cfgreg_cfg[(2*CFGREG_WIDTH)-1:(1*CFGREG_WIDTH)] :
														  (cfgreg_en_int == 4'b0100) ? cfgreg_cfg[(3*CFGREG_WIDTH)-1:(2*CFGREG_WIDTH)] :
														  							   cfgreg_cfg[(4*CFGREG_WIDTH)-1:(3*CFGREG_WIDTH)]; //4'b1000
								end
								else begin
									nxt_dout2matrix_ctl = ram_dout;
								end
								nxt_cycle_ctr			= CYCLE2;
							end
						CYCLE2: begin
								if (cfgreg_or_ram) begin
									//wait
								end
								else begin
									nxt_ram_write		= `TRUE;
								end
								nxt_cycle_ctr			= CYCLE3;
							end
						CYCLE3: begin
								if (cfgreg_or_ram) begin
									nxt_cfgreg_en		= cfgreg_en_int;
								end
								else begin
									nxt_ram_addr		= adder_incremented[MEMORY_ADDR_BITS-1:0];
								end
								//next trigger from matrix_ctl must be now
							end
					endcase
				end
	endcase
end

endmodule
