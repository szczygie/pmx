`include "pixel_matrix_parameters"
`include "column_ctl_params"

`timescale `TIMESCALE

module matrix_ctl (
		input clk,
		input reset,
		input [`MEMORY_WIDTH-1:0] in_data_to_pix,
		input [`CONTROL_WIDTH-1:0] in_control,
		input in_mem_reg_en,
		input [`MEMORY_DEPTH_WIDTH-1:0] in_number_of_mem,

		output [`MEMORY_WIDTH-1:0] out_data_from_pix,
		output out_finished,

		input [`COLUMNS_IN_MATRIX*`MEMORY_WIDTH-1:0] inpix_data_from_pix,

		output [`COLUMNS_IN_MATRIX*`MEMORY_WIDTH-1:0] outpix_data_to_pix,
		output [`COLUMNS_IN_MATRIX*`PIX_ADDR_WIDTH-1:0] outpix_pixel_address,
		output [`COLUMNS_IN_MATRIX-1:0] outpix_pixel_trigger,
		output [`COLUMNS_IN_MATRIX-1:0] outpix_mem_reg_en,
		output [`COLUMNS_IN_MATRIX*`PIX_CTL_WIDTH-1:0] outpix_control
	);

//--Multiplexer to Main CTL--//
	wire [`MEMORY_WIDTH-1:0] multiplex2main_data_from_pix;
	wire multiplex2main_done;

//--Main CTL to Column CTL--//
	wire [`MEMORY_WIDTH-1:0] main2col_data_to_pix;
	wire [`CONTROL_WIDTH-1:0] main2col_control;
	wire main2col_mem_reg_en;
	wire [`COLUMN_ADDR_WIDTH-1:0] main2col_column_address;
	wire [`MEMORY_DEPTH_WIDTH-1:0] main2col_number_of_mem_cells;

//--Column CTL to Multiplexer--//
	wire [`COLUMNS_IN_MATRIX*`MEMORY_WIDTH-1:0] col2multiplex_data_from_pix;
	wire [`COLUMNS_IN_MATRIX-1:0] col2multiplex_done;

//--Column Address delayed by the same number of clk cycles as data from pixels
	wire [`COLUMN_ADDR_WIDTH-1:0] delayed2_column_address;
	wire [`COLUMN_ADDR_WIDTH-1:0] delayed3_column_address;
	wire delayed1_multiplex2main_done;
	//wire column_done;
	

	main_ctl modul_zarzadzajacy (
		.clk (clk),
		.reset(reset),
		.column_addr_out (main2col_column_address),
		.control_in (in_control),
		.control_out (main2col_control),
		.data_from_pix_in (multiplex2main_data_from_pix),
		.data_from_pix_out(out_data_from_pix),
		.data_to_pix_in (in_data_to_pix),
		.data_to_pix_out (main2col_data_to_pix),
		.done (delayed1_multiplex2main_done),
		//.done (column_done),
		.mem_reg_en_in (in_mem_reg_en),
		.mem_reg_en_out (main2col_mem_reg_en),
		.number_of_mem_in (in_number_of_mem),
		.number_of_mem_out(main2col_number_of_mem_cells),
		.state_finished(out_finished)
	);


	column_ctl_array #(
		.NUMBER_OF_COLUMNS(`COLUMNS_IN_MATRIX)
	)
	tablica_kontrolerow_kolumny (
		.clk (clk),
		.reset(reset),
		.column_address (main2col_column_address),
		.control_in (main2col_control),
		.control_out (outpix_control),
		.data_from_pix_in (inpix_data_from_pix),
		.data_from_pix_out (col2multiplex_data_from_pix),
		.data_to_pix_in (main2col_data_to_pix),
		.data_to_pix_out (outpix_data_to_pix),
		.done (col2multiplex_done),
		.mem_reg_en_in (main2col_mem_reg_en),
		.mem_reg_en_out (outpix_mem_reg_en),
		.number_of_mem_cells(main2col_number_of_mem_cells),
		.pixel_address (outpix_pixel_address),
		.pixel_trigger (outpix_pixel_trigger)
	);

delay #(
	.DELAY_NR    (1),
	.SIGNAL_WIDTH(1)
)
col_done_delay (
	.clk            (clk),
	.delayed_signal (delayed1_multiplex2main_done),
	.signal_to_delay(multiplex2main_done)
	);
	
delay #(
	.DELAY_NR    (2),
	.SIGNAL_WIDTH(`COLUMN_ADDR_WIDTH)
)
col_addr_delay (
	.clk            (clk),
	.delayed_signal (delayed2_column_address),
	.signal_to_delay(main2col_column_address)
);

delay #(
	.DELAY_NR    (1),
	.SIGNAL_WIDTH(`COLUMN_ADDR_WIDTH)
)
col_addr_delay3 (
	.clk            (clk),
	.delayed_signal (delayed3_column_address),
	.signal_to_delay(delayed2_column_address)
);


	multiplexer #(
		.INPUTS_NR (`COLUMNS_IN_MATRIX),
		.INPUT_WIDTH (1),
		.SELECT_WIDTH(`COLUMN_ADDR_WIDTH)
	)
	done_multiplexer (
		.din (col2multiplex_done),
		.dout (multiplex2main_done),
		.select(delayed2_column_address)
	);

	//multiplexer #(
	//	.INPUTS_NR (2),
	//	.INPUT_WIDTH (1),
	//	.SELECT_WIDTH(1)
	//)
	//column_done_multiplexer (
	//	//.din ({delayed1_multiplex2main_done,multiplex2main_done}),
	//	.din ({delayed1_multiplex2main_done,delayed1_multiplex2main_done}),
	//	.dout (column_done),
	//	.select(outpix_control[(delayed2_column_address+1)*`PIX_CTL_WIDTH-1 -: `PIX_CTL_WIDTH] == `PIX_READ_AND_RESET)
	//);

	multiplexer #(
		.INPUTS_NR (`COLUMNS_IN_MATRIX),
		.INPUT_WIDTH (`MEMORY_WIDTH),
		.SELECT_WIDTH(`COLUMN_ADDR_WIDTH)
	)
	data_from_pix_multiplexer (
		.din (col2multiplex_data_from_pix),
		.dout (multiplex2main_data_from_pix),
		.select(delayed3_column_address)		
		);
	
endmodule
