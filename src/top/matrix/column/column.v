// History:
// 2019-12-14 RSz. Reset buffer for each column separately

`timescale 1ns/1ns

`include "column_ctl_params"

module column(
    input  wire                       clk,
    input  wire                       rst,
    input  wire                       b_strobe,
    input  wire [`PIX_ADDR_WIDTH-1:0] addr_in,
    input  wire                       cfgreg_or_ram,
    input  wire [`MEMORY_WIDTH-1:0]   din,
    output wire [`MEMORY_WIDTH-1:0]   dout,
    input  wire [`PIX_CTL_WIDTH-1:0]  op_mode,
    input  wire                       pixel_trigger
);

wire   [`PIX_IN_COLUMN*`MEMORY_WIDTH-1:0] col_data_from_pix;

genvar                                    i;
generate
    for (i=0; i<`PIX_IN_COLUMN; i=i+1) begin: pixel

        pixel #(
            .ADDR (i),
            .ADDR_WIDTH (`PIX_ADDR_WIDTH),
            .CTL_WIDTH (`PIX_CTL_WIDTH),
            .MEMORY_ADDR_BITS (`MEMORY_ADDR_BITS),
            .MEMORY_WIDTH (`MEMORY_WIDTH),
            .MEMORY_DEPTH (`MEMORY_DEPTH),
            .CFGREG_REG_NUMBER(`CFGREG_DEPTH),
            .CFGREG_WIDTH (`CFGREG_WIDTH)/*,
         .SEED         ((i+3)*(j+5)^2+1)*/
        )
        u_pixel (
            .addr_in (addr_in),            //pixel address from matrix_ctl
            .cfgreg_or_ram(cfgreg_or_ram), //0-RAM, 1-CfgReg
            .clk (clk),                    //clk
            .rst (rst),
            .din (din),                    //data to memory or to CfgReg
            .dout (col_data_from_pix[(i+1)*`MEMORY_WIDTH-1 -: `MEMORY_WIDTH]), //data from RAM or CfgReg to matrix_ctl
            .op_mode(op_mode),             //operation mode: 00 IDLE, 01 WRITE, 10 READ_AND_RST, 11 READ
            .pixel_trigger(pixel_trigger), //matrix_ctl puts '1' here to indicate that pixel should operate
            .b_strobe(b_strobe)
        );
    end
endgenerate

multiplexer #(
    .INPUTS_NR (`PIX_IN_COLUMN),
    .INPUT_WIDTH (`MEMORY_WIDTH),
    .SELECT_WIDTH(`PIX_ADDR_WIDTH)
)
col_data_from_pix_multiplexer (
    .din (col_data_from_pix),
    .select(addr_in),
    .dout (dout)
);

endmodule
