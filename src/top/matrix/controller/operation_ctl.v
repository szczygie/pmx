`include "pixel_matrix_parameters"
`include "column_ctl_params"

`timescale `TIMESCALE

module operation_ctl 
	#(
	parameter COLUMN_ADDR = 1,

	COLUMN_RESET_STATE     	   = 3'b000,
	COLUMN_WRITE_STATE               = 3'b001,
	COLUMN_READ_STATE                = 3'b011,
	COLUMN_READ_AND_RESET_STATE      = 3'b010,
	COLUMN_READ_AND_RESET_PART_STATE = 3'b110,
	COLUMN_READ_PART_STATE           = 3'b111,
	COLUMN_WRITE_PART_STATE          = 3'b101,
	COLUMN_COLLECT_DATA_STATE        = 3'b100
	)
	(
//--Inputs--//
	input clk,										//Clock signal
	input reset,										//Reset signal
	input [`COLUMN_ADDR_WIDTH-1:0] column_address, 	//Address of active column
	input [`CONTROL_WIDTH-1:0] control_in, 			//Column CTL control signal
	input [`MEMORY_WIDTH-1:0] data_to_pix_in, 		//Data to be send to pixel
	input [`MEMORY_WIDTH-1:0] data_from_pix_in, 	//Data from pixel to be send from CTL
	input mem_reg_en_in, 							//Memory or Control Register enable
	input [`MEMORY_DEPTH_WIDTH-1:0] number_of_mem_cells,	//Number of memory cells to operate on in partial read/write mode
	input done, 									//Operation done, go to RESET state
//--Outputs--//
	output reg [`MEMORY_WIDTH-1:0] data_to_pix, 							//Data sent to pixel
	output reg [`MEMORY_WIDTH-1:0] data_from_pix,							//Data received from pixel
	output reg [`MEMORY_DEPTH_WIDTH-1:0] memory_cells,						//Number of memory/cfg_reg cell to perform operation on
	output reg [`PIX_CTL_WIDTH-1:0] control,								//Pixel state signal
	output reg [`MEMORY_OPERATION_CLK_CYCELS_WIDTH-1:0] operation_duration,	//Number of cycles of operation performance
	output reg mem_reg_en													//Select between 1'b0 - memory and 1'b1 - cfg_reg
		);


//--Variables--//
	reg [`CONTROL_WIDTH-1:0] state;
	reg [`CONTROL_WIDTH-1:0] state_next;

//--State register--//
	always@(posedge clk) begin: State_register
		if(reset)
			state <= COLUMN_RESET_STATE;
		else
			state <= state_next;
	end

//--Next state logic--//
	always @* begin: Next_state_logic
		//--Default state--//
		state_next = COLUMN_RESET_STATE;

		case(state)
			COLUMN_RESET_STATE: begin

				state_next = COLUMN_RESET_STATE;
				
				if(control_in == `COLUMN_COLLECT_DATA_COMMAND) begin
					state_next = COLUMN_COLLECT_DATA_STATE;
				end
				else if(COLUMN_ADDR == column_address) begin
					case(control_in)
						`COLUMN_WRITE_COMMAND: state_next = COLUMN_WRITE_STATE;
						`COLUMN_READ_COMMAND: state_next = COLUMN_READ_STATE;
						`COLUMN_READ_AND_RESET_COMMAND:	state_next = COLUMN_READ_AND_RESET_STATE;
						`COLUMN_WRITE_PART_COMMAND: state_next = COLUMN_WRITE_PART_STATE;
						`COLUMN_READ_AND_RESET_PART_COMMAND: state_next = COLUMN_READ_AND_RESET_PART_STATE;
						`COLUMN_READ_PART_COMMAND: state_next = COLUMN_READ_PART_STATE;
					endcase
				end	
			end

			COLUMN_COLLECT_DATA_STATE: begin
				state_next = COLUMN_COLLECT_DATA_STATE;

				if(control_in == `COLUMN_RESET_COMMAND)begin
					state_next = COLUMN_RESET_STATE;
				end
			end
			
			COLUMN_WRITE_STATE,
			COLUMN_READ_STATE,
			COLUMN_READ_AND_RESET_STATE,
			COLUMN_WRITE_PART_STATE,
			COLUMN_READ_PART_STATE,
			COLUMN_READ_AND_RESET_PART_STATE:
			begin
				state_next = state;

				if(done) begin
					state_next = COLUMN_RESET_STATE;
				end
			end
		endcase
	end

//--Output logic--//
	always @* begin: Output_logic
		//--Defualt settings--//
		data_to_pix   = {`MEMORY_WIDTH{1'b0}};
		data_from_pix = {`MEMORY_WIDTH{1'b0}};
		memory_cells  = {`MEMORY_DEPTH_WIDTH{1'b0}};
		control       = `PIX_IDLE;
		mem_reg_en    = 1'b0;
		operation_duration = {`MEMORY_OPERATION_CLK_CYCELS_WIDTH{1'b0}};

		case(state)
			COLUMN_COLLECT_DATA_STATE:begin
				data_to_pix   = {`MEMORY_WIDTH{1'b0}};
				data_from_pix = {`MEMORY_WIDTH{1'b0}};
				memory_cells  = {`MEMORY_DEPTH_WIDTH{1'b0}};
				control       = `PIX_COLLECT_DATA;
				mem_reg_en    = 1'b0;
				operation_duration = {`MEMORY_OPERATION_CLK_CYCELS_WIDTH{1'b0}};
			end
			COLUMN_WRITE_STATE: begin
				data_to_pix   = data_to_pix_in;
				data_from_pix = {`MEMORY_WIDTH{1'b0}};
				memory_cells  = mem_reg_en_in ? `CFGREG_DEPTH : `MEMORY_DEPTH;
				control       = `PIX_WRITE;
				operation_duration = mem_reg_en_in ? `PIX_CFGREG_WRITE_DURATION : `PIX_MEM_WRITE_DURATION;
				mem_reg_en = mem_reg_en_in;
			end
			COLUMN_READ_STATE: begin
				data_to_pix   = {`MEMORY_WIDTH{1'b0}};
				data_from_pix = data_from_pix_in;
				memory_cells  = mem_reg_en_in ? `CFGREG_DEPTH : `MEMORY_DEPTH;
				control       = `PIX_READ;
				operation_duration = mem_reg_en_in ? `PIX_CFGREG_READ_DURATION : `PIX_MEM_READ_DURATION;
				mem_reg_en = mem_reg_en_in;
			end
			COLUMN_READ_AND_RESET_STATE: begin
				data_to_pix   = {`MEMORY_WIDTH{1'b0}};
				data_from_pix = data_from_pix_in;
				memory_cells  = mem_reg_en_in ? `CFGREG_DEPTH : `MEMORY_DEPTH;
				control       = `PIX_READ_AND_RESET;
				operation_duration = mem_reg_en_in ? `PIX_CFGREG_READ_AND_RESET_DURATION : `PIX_MEM_READ_AND_RESET_DURATION;
				mem_reg_en = mem_reg_en_in;
			end
			COLUMN_WRITE_PART_STATE: begin
				data_to_pix   = data_to_pix_in;
				data_from_pix = {`MEMORY_WIDTH{1'b0}};
				memory_cells  = mem_reg_en_in ? `CFGREG_DEPTH : number_of_mem_cells;
				control       = `PIX_WRITE;
				operation_duration = mem_reg_en_in ? `PIX_CFGREG_WRITE_DURATION : `PIX_MEM_WRITE_DURATION;
				mem_reg_en = mem_reg_en_in;
			end
			COLUMN_READ_PART_STATE:begin
				data_to_pix   = {`MEMORY_WIDTH{1'b0}};
				data_from_pix = data_from_pix_in;
				memory_cells  = mem_reg_en_in ? `CFGREG_DEPTH : number_of_mem_cells;
				control       = `PIX_READ;
				operation_duration = mem_reg_en_in ? `PIX_CFGREG_READ_DURATION : `PIX_MEM_READ_DURATION;
				mem_reg_en = mem_reg_en_in;
			end
			COLUMN_READ_AND_RESET_PART_STATE: begin
				data_to_pix   = {`MEMORY_WIDTH{1'b0}};
				data_from_pix = data_from_pix_in;
				memory_cells  = mem_reg_en_in ? `CFGREG_DEPTH : number_of_mem_cells;
				control       = `PIX_READ_AND_RESET;
				operation_duration = mem_reg_en_in ? `PIX_CFGREG_READ_AND_RESET_DURATION : `PIX_MEM_READ_AND_RESET_DURATION;
				mem_reg_en = mem_reg_en_in;
			end
		endcase
	end
endmodule
