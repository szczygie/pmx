`include "pixel_matrix_parameters"
`include "column_ctl_params"

`timescale `TIMESCALE

module column_ctl_array
	#(
		parameter NUMBER_OF_COLUMNS = 2	
	)
	(
		//outside_interface
		input clk,
		input reset,
		input [`COLUMN_ADDR_WIDTH-1:0] column_address,
		input [`CONTROL_WIDTH-1:0] control_in,
		input [`MEMORY_WIDTH-1:0] data_to_pix_in,
		input [`MEMORY_DEPTH_WIDTH-1:0] number_of_mem_cells,
		input mem_reg_en_in,
		
		//pixel_interface
		input [NUMBER_OF_COLUMNS*`MEMORY_WIDTH-1:0] data_from_pix_in,
		output [NUMBER_OF_COLUMNS*`MEMORY_WIDTH-1:0] data_from_pix_out,
		output [NUMBER_OF_COLUMNS*`MEMORY_WIDTH-1:0] data_to_pix_out,
		output [NUMBER_OF_COLUMNS*`PIX_CTL_WIDTH-1:0] control_out,
		output [NUMBER_OF_COLUMNS*`PIX_ADDR_WIDTH-1:0] pixel_address,
		output [NUMBER_OF_COLUMNS-1:0] pixel_trigger,
		output [NUMBER_OF_COLUMNS-1:0] mem_reg_en_out,
		output [NUMBER_OF_COLUMNS-1:0] done
	);

genvar i;
generate
	begin
		for(i = 0; i < NUMBER_OF_COLUMNS; i = i + 1) begin
			column_ctl #(
				.COLUMN_ADDR(i)
			)
			kontroler_kolumny (
				.clk              (clk),
				.reset            (reset),
				.column_address   (column_address), //Address of active column
				.control_in       (control_in), //Column CTL control signal
				.control_out      (control_out[(i+1)*`PIX_CTL_WIDTH-1 -: `PIX_CTL_WIDTH]), //Pixel control signal
				.data_from_pix_in (data_from_pix_in[(i+1)*`MEMORY_WIDTH-1 -:`MEMORY_WIDTH]), //Data from pixel to be send from CTL
				.data_from_pix_out(data_from_pix_out[(i+1)*`MEMORY_WIDTH-1 -:`MEMORY_WIDTH]), //Data from pixel sent from CTL
				.data_to_pix_in   (data_to_pix_in), //Data to be send to pixel
				.data_to_pix_out  (data_to_pix_out[(i+1)*`MEMORY_WIDTH-1 -: `MEMORY_WIDTH]), //Data sent to pixel
				.increment_column_address(done[i]),
				.mem_reg_en_in    (mem_reg_en_in), //Select between 1'b0 - memory and 1'b1 - cfg_reg
				.mem_reg_en_out   (mem_reg_en_out[i]), //Buffered select between 1'b0 - memory and 1'b1 - cfg_reg
				.number_of_mem_cells(number_of_mem_cells),
				.pixel_address    (pixel_address[(i+1)*`PIX_ADDR_WIDTH-1 -: `PIX_ADDR_WIDTH]), //Active pixel in column
				.pixel_trigger    (pixel_trigger[i]) //Trigger operation performing in pixel
				);
		end
	end
endgenerate
		

endmodule
