/******************************************************************************
 * DVT CODE TEMPLATE: env
 * Created by szczygie on Oct 7, 2019
 * uvc_company = kmie, uvc_name = pmx
 *******************************************************************************/

`ifndef IFNDEF_GUARD_kmie_pmx_env
`define IFNDEF_GUARD_kmie_pmx_env

//------------------------------------------------------------------------------
//
// CLASS: kmie_pmx_env
//
//------------------------------------------------------------------------------

class kmie_pmx_env extends uvm_env;

    // Components of the environment
    kmie_pmx_agent m_kmie_pmx_agent;
    kmie_pmx_scoreboard m_scoreboard;
    kmie_pmx_config_obj m_config_obj;
    kmie_pmx_ref_model_wrp ref_model;

    `uvm_component_utils(kmie_pmx_env)

    function new(string name, uvm_component parent);
        super.new(name, parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);

        begin
            // Create the configuration object if it has not been set
            if(!uvm_config_db#(kmie_pmx_config_obj)::get(this, "", "m_config_obj", m_config_obj)) begin
                m_config_obj = kmie_pmx_config_obj::type_id::create("m_config_obj", this);
                uvm_config_db#(kmie_pmx_config_obj)::set(this, {"m_kmie_pmx_agent","*"}, "m_config_obj", m_config_obj);
                uvm_config_db#(kmie_pmx_config_obj)::set(this, {"m_scoreboard","*"}, "m_config_obj", m_config_obj);
            end

            // Create the agent
            m_kmie_pmx_agent = kmie_pmx_agent::type_id::create("m_kmie_pmx_agent", this);

            // Create scoreboard
            if(m_config_obj.m_checks_enable) begin
                m_scoreboard = kmie_pmx_scoreboard::type_id::create("m_scoreboard", this);
                ref_model = kmie_pmx_ref_model_wrp::type_id::create("ref_model", this);
            end

        end

    endfunction : build_phase

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        if(m_config_obj.m_checks_enable) begin
            // reset connections
            m_kmie_pmx_agent.m_monitor_in.m_reset_port.connect(m_scoreboard.m_reset_fifo.analysis_export);
            m_kmie_pmx_agent.m_monitor_in.m_reset_port.connect(ref_model.m_reset_fifo.analysis_export);
            // data packets: monitor_in -> ref_model -> scoreboard; monitor_out -> scoreboard
            m_kmie_pmx_agent.m_monitor_in.m_collected_item_port.connect(ref_model.m_in_fifo.analysis_export);
            ref_model.m_out_item_ap.connect(m_scoreboard.m_in_fifo.analysis_export);
            m_kmie_pmx_agent.m_monitor_out.m_collected_item_port.connect(m_scoreboard.m_out_fifo.analysis_export);
        end
    endfunction : connect_phase

endclass : kmie_pmx_env

`endif // IFNDEF_GUARD_kmie_pmx_env
